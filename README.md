# Hawai AGP

Pour installer l'application :

* Créer une base mysql et préciser la configuration (connection.cfg.xml)
* Lancer la classe LoadData (qui charge les données en base)
* Préciser le path des libraries => WEB-INF/lib
* Lancer l'application sur un server Tomcat 7.0
* URL : http://localhost:8080/<nom_du_repertoire>/main.jsf
