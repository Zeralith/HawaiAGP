package bean;

import java.util.ArrayList;
import java.util.List;

import buisness.model.Hotel;
import buisness.model.Offer;
import buisness.model.Restaurant;
import buisness.model.TouristAttraction;
import persistence.hibernate.ORMBuilder;

public class Tools {
	
	
	private static Tools INSTANCE = null;
	private ArrayList<Offer> offers;
	private List<Hotel> hotelList = new ArrayList<Hotel>();
	private List<TouristAttraction> touristAttractionList = new ArrayList<TouristAttraction>();
	private List<Restaurant> restaurantList = new ArrayList<Restaurant>();
	private ORMBuilder ormBuilder = new ORMBuilder();
	
	
	public static Tools getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Tools();
		}
		
		return INSTANCE;
	}
	
	private Tools(){ }
	
	public String getStringTime(Double hours){
		double hoursFin = Math.ceil(hours);
		double minFin = Math.ceil(( (hours-Math.ceil(hours))*60 ));
		
		String str = ""+ (int)hoursFin + "h " + (int)minFin  + " min";
		
		return str;
	}
	
	

	public ArrayList<Offer> getOffers() {
		return offers;
	}

	public void setOffers(ArrayList<Offer> offers) {
		this.offers = offers;
	}
	
	public void loadData(){
		touristAttractionList= ormBuilder.buildTouristAttractionData();
	    hotelList= ormBuilder.buildHotelData();   
	    restaurantList= ormBuilder.buildResturantData();
	}

	public List<Hotel> getHotelList() {
		return hotelList;
	}

	public void setHotelList(List<Hotel> hotelList) {
		this.hotelList = hotelList;
	}

	public List<TouristAttraction> getTouristAttractionList() {
		return touristAttractionList;
	}

	public void setTouristAttractionList(List<TouristAttraction> touristAttractionList) {
		this.touristAttractionList = touristAttractionList;
	}

	public List<Restaurant> getRestaurantList() {
		return restaurantList;
	}

	public void setRestaurantList(List<Restaurant> restaurantList) {
		this.restaurantList = restaurantList;
	}
	
}
