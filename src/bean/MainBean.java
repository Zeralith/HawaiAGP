package bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.richfaces.model.Filter;

import buisness.engine.CreateOffers;
import buisness.model.Excursion;
import buisness.model.Hotel;
import buisness.model.Offer;
import buisness.model.Restaurant;
import buisness.model.TouristAttraction;
import buisness.model.Travel;
import persistence.hibernate.ORMBuilder;


public class MainBean {
	private List<Excursion> excursionListForOffer = new ArrayList<Excursion>(); ;
	private ORMBuilder ormBuilder = new ORMBuilder();
	private CreateOffers createOffers = CreateOffers.getInstance();
	private Map<String, Excursion> excursionTmpMap;
	private List<Hotel> hotelList = new ArrayList<Hotel>();
	private List<Offer> offerList = new ArrayList<Offer>();
	private List<String> islandNameList = new ArrayList<String>();
	private int starsFilter;
	private String islandFilter;
	private String hotelNameFilter;
	private List<Offer> offerListBis = new ArrayList<Offer>();
	
	private List<String> latitudeList1 = new ArrayList<String>();
	private List<String> longitudeList1 = new ArrayList<String>();
	private List<String> latitudeList2 = new ArrayList<String>();
	private List<String> longitudeList2 = new ArrayList<String>();
	private String latitude;
	private String longitude;
	
	private String currentIsland;
	private String currentType;
	private String currentChoice;
	private List<SelectItem> islandList = new ArrayList<SelectItem>();
	private List<SelectItem> attractionList = new ArrayList<SelectItem>();
	private List<SelectItem> choiceList = new ArrayList<SelectItem>();
	
	private List<String> critereList = new ArrayList<String>();

	private List<Restaurant> restaurantList ;//= new ArrayList<Restaurant>();
	private List<String> restaurantTypeList = new ArrayList<String>();
	private String restaurantTypeFilter;
	private int starsRestaurantFilter;
	private String restaurantNameFilter;
	
	private List<TouristAttraction> touristAttractionList = new ArrayList<TouristAttraction>();
	private List<String> typeTouristAttractionList = new ArrayList<String>();
	private String touristAttractionIslandFilter;
	private int timeVisiteTouristAttractionFilter;
	private String nameTouristAttractionFilter;
	private String touristAttractionTypeFilter;
	
	private boolean rowExcursionIsSelected;
	
	private boolean activateButtonOffer;
	private boolean rowIsSelected;
	private boolean rowRestaurantIsSelected;
	private boolean rowTouristAttractionIsSelected;
	private int currentTouristAttractionIndex;
	private int currentHotelIndex;
	private int currentRestaurantIndex;
	
	private String selectedExcursion;
	private Excursion excursion;
	
	private String selectedRowId;
	private List<Travel> travelFromMap = new ArrayList<Travel>(); 
	private List<String> day = new ArrayList<String>(); 
    private String pattern;
    private Date selectedArrivalDate;
    private Date selectedDepartureDate;
    private DateTimeZone FRANCE = DateTimeZone.forID("Europe/Paris");
    
    private String keyWord;
    private List<String> keyWordList = new ArrayList<String>();
    private String idTmpOffer;
    private TouristAttraction currentTouristAttraction;

	public MainBean() {
	}
	
	@PostConstruct
	public void init() {
		pattern = "MMM d, yyyy";
 		currentChoice="";
		currentIsland = "";
		currentType="";
		rowIsSelected=false;
		selectedRowId="";
		rowIsSelected = false;
		rowRestaurantIsSelected = false;
		rowExcursionIsSelected = false;
		rowTouristAttractionIsSelected = false;
		activateButtonOffer=true;
		
		islandNameList.add("");
		islandNameList.add("ile Hawaii");
	    islandNameList.add("ile Maui");
	    islandNameList.add("ile Oahu");
	    islandNameList.add("ile Kauai");
	    islandNameList.add("ile Molokai");

	    
	    
	    touristAttractionList= Tools.getInstance().getTouristAttractionList();
	    hotelList= Tools.getInstance().getHotelList(); 
	    restaurantList= Tools.getInstance().getRestaurantList();
	    
	    if(touristAttractionList.size() < 2){
	    	touristAttractionList= ormBuilder.buildTouristAttractionData();
	 	    hotelList= ormBuilder.buildHotelData(); 
	 	    restaurantList= ormBuilder.buildResturantData();
	 	    
	 	    Tools.getInstance().setHotelList(hotelList);
	 	    Tools.getInstance().setRestaurantList(restaurantList);
	 	    Tools.getInstance().setTouristAttractionList(touristAttractionList);
	    }
	    
		typeTouristAttractionList.add("");
		typeTouristAttractionList.add("Loisir");
		typeTouristAttractionList.add("Historique");
		
		SelectItem item = new SelectItem("", "");
	    for(int index = 0 ; index < islandNameList.size() ; index++){
	        item = new SelectItem(islandNameList.get(index), islandNameList.get(index));
	        islandList.add(item);
	    }
	    for(int index = 0 ; index < typeTouristAttractionList.size() ; index++){
	        item = new SelectItem(typeTouristAttractionList.get(index), typeTouristAttractionList.get(index));
	        attractionList.add(item);
	    }
	    choiceList.add(new SelectItem("Oui", "Oui"));
	    choiceList.add(new SelectItem("Non", "Non"));	
		
		day.add("Lundi");
		day.add("Mardi");
		day.add("Mercredi");
		day.add("Jeudi");
		day.add("Vendredi");
		day.add("Samedi");
		day.add("Dimanche");
		
		
		
		restaurantTypeList.add("");
		restaurantTypeList.add("Americaine");
		restaurantTypeList.add("Japonaise");
		restaurantTypeList.add("Hawaiienne");
		restaurantTypeList.add("Italien");
		restaurantTypeList.add("Vegetarien");
		restaurantTypeList.add("Asiatique Vietnamien");
		restaurantTypeList.add("Mexicaine");
		
		
		
	}	
	
	
	
	public Filter<?> getStarsFilterImpl() {
        return new Filter<Hotel>() {
            public boolean accept(Hotel item) {
                Integer stars = getStarsFilter();
                if ( stars == null || stars == 0 || stars == item.getNumberOfStars()) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getTimeVisiteTouristAttractionFilterImpl() {
        return new Filter<TouristAttraction>() {
            public boolean accept(TouristAttraction item) {
                Integer time = getTimeVisiteTouristAttractionFilter();
                if ( time == null || time == 0 || time == item.getTimeVisiteTouristAttraction()) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getStarsRestaurantFilterImpl() {
        return new Filter<Restaurant>() {
            public boolean accept(Restaurant item) {
                Integer stars = getStarsFilter();
                if ( stars == null || stars == 0 || stars == item.getStars()) {
                    return true;
                }
                return false;
            }
        };
    } 
	public void excursionSelected() {
		
			rowExcursionIsSelected = true;
			rowIsSelected = false;
			rowRestaurantIsSelected = false;
			rowTouristAttractionIsSelected = false;
			
			
			
			excursionListForOffer = getExcursionS(Integer.parseInt(idTmpOffer));
			
			this.travelFromMap = new ArrayList<Travel>(); 
			
			for(int i=0; i<excursionListForOffer.size();i++){
				if(excursionListForOffer.get(i).getLunchRestaurant().getNameRestaurant().equals(selectedExcursion)){
					
					this.excursion=excursionListForOffer.get(i);
				}
			}

			for(int i=0 ; i < excursion.getTravelsHashMap().size() ; i++){
				
				if(excursion.getTravelsHashMap().get(i) != null){
					this.travelFromMap.add(excursion.getTravelsHashMap().get(i));
				if(this.travelFromMap.get(i).isHotelToTouristAttraction()){
	
					latitudeList1.add(this.travelFromMap.get(i).getHotelB().getGpsCoordinatesHotel().split(",")[0]);
					latitudeList2.add(this.travelFromMap.get(i).getTouristAttractionA().getGpsCoordinatesTouristAttraction().split(",")[0]);
					longitudeList1.add(this.travelFromMap.get(i).getHotelB().getGpsCoordinatesHotel().split(",")[1]);
					longitudeList2.add(this.travelFromMap.get(i).getTouristAttractionA().getGpsCoordinatesTouristAttraction().split(",")[1]);}
				else{
					latitudeList1.add(this.travelFromMap.get(i).getTouristAttractionA().getGpsCoordinatesTouristAttraction().split(",")[0]);
					longitudeList1.add(this.travelFromMap.get(i).getTouristAttractionA().getGpsCoordinatesTouristAttraction().split(",")[1]);
					if(!this.travelFromMap.get(i).getHotelB().equals(null)){
						latitudeList2.add(this.travelFromMap.get(i).getHotelB().getGpsCoordinatesHotel().split(",")[0]);
						longitudeList2.add(this.travelFromMap.get(i).getHotelB().getGpsCoordinatesHotel().split(",")[1]);
				}
					else{
						latitudeList2.add(this.travelFromMap.get(i).getTouristAttractionB().getGpsCoordinatesTouristAttraction().split(",")[0]);
						longitudeList2.add(this.travelFromMap.get(i).getTouristAttractionB().getGpsCoordinatesTouristAttraction().split(",")[1]);
					}
				}
				}
		}
	}
	
	public void rowSelected() {
		
		System.out.println("caca1caca1caca1");
		
			rowIsSelected = true;
			rowRestaurantIsSelected = false;
			rowTouristAttractionIsSelected = false;
			rowExcursionIsSelected = false;
		
		
		currentTouristAttraction=touristAttractionList.get(currentTouristAttractionIndex);
		latitude=currentTouristAttraction.getGpsCoordinatesTouristAttraction().split(",")[0];
		longitude=currentTouristAttraction.getGpsCoordinatesTouristAttraction().split(",")[1];
		
	}
	
	public void rowAttractionSelected() {
		
		
		for(int i = 0 ; i< touristAttractionList.size() ; i++){
			if(touristAttractionList.get(i).getNameTouristAttraction()==selectedRowId){
				currentTouristAttraction=touristAttractionList.get(i);
			}
		}
		if(selectedRowId.length()!=0){
			rowIsSelected = true;
			rowRestaurantIsSelected = false;
			rowTouristAttractionIsSelected = false;
			rowExcursionIsSelected = false;
		}
	}
	
	public void rowRestaurantSelected() {
		rowRestaurantIsSelected = true;
		rowIsSelected = false;
		rowTouristAttractionIsSelected = false;
		rowExcursionIsSelected = false;
	}
	
	public void rowTouristAttractionSelected() {
		rowRestaurantIsSelected = false;
		rowIsSelected = false;
		rowTouristAttractionIsSelected = true;
		rowExcursionIsSelected = false;
	}

	public List<Hotel> getHotelList() {
		return hotelList;
	}

	public void setHotelList(List<Hotel> hotelList) {
		this.hotelList = hotelList;
	}

	public String getIslandFilter() {
		return islandFilter;
	}

	public void setIslandFilter(String islandFilter) {
		this.islandFilter = islandFilter;
	}

	public List<String> getIslandNameList() {
		return islandNameList;
	}

	public void setIslandNameList(List<String> islandNameList) {
		this.islandNameList = islandNameList;
	}

	public int getStarsFilter() {
		return starsFilter;
	}

	public void setStarsFilter(int starsFilter) {
		this.starsFilter = starsFilter;
	}

	public String getHotelNameFilter() {
		return hotelNameFilter;
	}

	public void setHotelNameFilter(String hotelNameFilter) {
		this.hotelNameFilter = hotelNameFilter;
	}

	public String getRestaurantTypeFilter() {
		return restaurantTypeFilter;
	}

	public void setRestaurantTypeFilter(String restaurantTypeFilter) {
		this.restaurantTypeFilter = restaurantTypeFilter;
	}

	public List<Restaurant> getRestaurantList() {
		return restaurantList;
	}

	public void setRestaurantList(List<Restaurant> restaurantList) {
		this.restaurantList = restaurantList;
	}

	public List<String> getRestaurantTypeList() {
		return restaurantTypeList;
	}

	public void setRestaurantTypeList(List<String> restaurantTypeList) {
		this.restaurantTypeList = restaurantTypeList;
	}

	public int getStarsRestaurantFilter() {
		return starsRestaurantFilter;
	}

	public void setStarsRestaurantFilter(int starsRestaurantFilter) {
		this.starsRestaurantFilter = starsRestaurantFilter;
	}

	public String getRestaurantNameFilter() {
		return restaurantNameFilter;
	}

	public void setRestaurantNameFilter(String restaurantNameFilter) {
		this.restaurantNameFilter = restaurantNameFilter;
	}

	public List<TouristAttraction> getTouristAttractionList() {
		return touristAttractionList;
	}

	public void setTouristAttractionList(List<TouristAttraction> touristAttractionList) {
		this.touristAttractionList = touristAttractionList;
	}

	public List<String> getTypeTouristAttractionList() {
		return typeTouristAttractionList;
	}

	public void setTypeTouristAttractionList(List<String> typeTouristAttractionList) {
		this.typeTouristAttractionList = typeTouristAttractionList;
	}

	public String getTouristAttractionIslandFilter() {
		return touristAttractionIslandFilter;
	}

	public void setTouristAttractionIslandFilter(String touristAttractionIslandFilter) {
		this.touristAttractionIslandFilter = touristAttractionIslandFilter;
	}

	public int getTimeVisiteTouristAttractionFilter() {
		return timeVisiteTouristAttractionFilter;
	}

	public void setTimeVisiteTouristAttractionFilter(int timeVisiteTouristAttractionFilter) {
		this.timeVisiteTouristAttractionFilter = timeVisiteTouristAttractionFilter;
	}

	public String getNameTouristAttractionFilter() {
		return nameTouristAttractionFilter;
	}

	public void setNameTouristAttractionFilter(String nameTouristAttractionFilter) {
		this.nameTouristAttractionFilter = nameTouristAttractionFilter;
	}

	public boolean isRowIsSelected() {
		return rowIsSelected;
	}

	public void setRowIsSelected(boolean rowIsSelected) {
		this.rowIsSelected = rowIsSelected;
	}

	public int getCurrentHotelIndex() {
		return currentHotelIndex;
	}

	public void setCurrentHotelIndex(int currentHotelIndex) {
		this.currentHotelIndex = currentHotelIndex;
	} 

	public String getTouristAttractionTypeFilter() {
		return touristAttractionTypeFilter;
	}

	public void setTouristAttractionTypeFilter(String touristAttractionTypeFilter) {
		this.touristAttractionTypeFilter = touristAttractionTypeFilter;
	}

	public int getCurrentRestaurantIndex() {
		return currentRestaurantIndex;
	}

	public void setCurrentRestaurantIndex(int currentRestaurantIndex) {
		this.currentRestaurantIndex = currentRestaurantIndex;
	}

	public boolean isRowRestaurantIsSelected() {
		return rowRestaurantIsSelected;
	}

	public void setRowRestaurantIsSelected(boolean rowRestaurantIsSelected) {
		this.rowRestaurantIsSelected = rowRestaurantIsSelected;
	}

	public boolean isRowTouristAttractionIsSelected() {
		return rowTouristAttractionIsSelected;
	}

	public void setRowTouristAttractionIsSelected(boolean rowTouristAttractionIsSelected) {
		this.rowTouristAttractionIsSelected = rowTouristAttractionIsSelected;
	}

	public int getCurrentTouristAttractionIndex() {
		return currentTouristAttractionIndex;
	}

	public void setCurrentTouristAttractionIndex(int currentTouristAttractionIndex) {
		this.currentTouristAttractionIndex = currentTouristAttractionIndex;
	}


	public List<Offer> getOfferList() {
		return offerList;
	}

	public void setOfferList(List<Offer> offerList) {
		this.offerList = offerList;
	}

	

	public Map<String, Excursion> getExcursionTmpMap() {
		return excursionTmpMap;
	}

	public void setExcursionTmpMap(Map<String, Excursion> excursionTmpMap) {
		this.excursionTmpMap = excursionTmpMap;
	}

	public List<Excursion> getDayEnum(org.richfaces.component.SequenceIterationStatus index) {
		
		offerList = Tools.getInstance().getOffers();
		
		
		Map<Integer, Excursion> excursionMap = offerList.get(index.getIndex()).getExcursionsHashMap();
		List<Excursion> dayEnum = new ArrayList<Excursion>();
		
		
		dayEnum.add(excursionMap.get(0));
		dayEnum.add(excursionMap.get(1));
		dayEnum.add(excursionMap.get(2));
		dayEnum.add(excursionMap.get(3));
		dayEnum.add(excursionMap.get(4));
		dayEnum.add(excursionMap.get(5));
		dayEnum.add(excursionMap.get(6));
		
		
		/*System.out.println(dayEnum.get(0).getTimeInHours());
		System.out.println(dayEnum.get(1).getTimeInHours());
		System.out.println(dayEnum.get(2).getTimeInHours());
		System.out.println(dayEnum.get(3).getTimeInHours());
		System.out.println(dayEnum.get(4).getTimeInHours());
		System.out.println(dayEnum.get(0).getTravelsHashMap().get(1));
		System.out.println(dayEnum.get(1).getTravelsHashMap().get(1));
		System.out.println(dayEnum.get(2).getTravelsHashMap().get(1));
		System.out.println(dayEnum.get(3).getTravelsHashMap().get(1));
		System.out.println(dayEnum.get(4).getTravelsHashMap().get(1));*/
		
		this.excursionListForOffer.addAll(dayEnum);
		
		return dayEnum;
	}
	
	
	public List<Excursion> getExcursionS(int index) {
		offerList = Tools.getInstance().getOffers();
		Map<Integer, Excursion> excursionMap = offerList.get(index).getExcursionsHashMap();
		List<Excursion> dayEnum = new ArrayList<Excursion>();
		
		
		dayEnum.add(excursionMap.get(0));
		dayEnum.add(excursionMap.get(1));
		dayEnum.add(excursionMap.get(2));
		dayEnum.add(excursionMap.get(3));
		dayEnum.add(excursionMap.get(4));
		dayEnum.add(excursionMap.get(5));
		dayEnum.add(excursionMap.get(6));
		
		return dayEnum;
	}
	
	public String getNameDay(org.richfaces.component.SequenceIterationStatus index) {
		return day.get(index.getIndex());
	}

	public String getSelectedRowId() {
		return selectedRowId;
	}

	public void setSelectedRowId(String selectedRowId) {
		this.selectedRowId = selectedRowId;
	}

	public List<String> getDay() {
		return day;
	}

	public void setDay(List<String> day) {
		this.day = day;
	}

	public String getCurrentIsland() {
		return currentIsland;
	}

	public void setCurrentIsland(String currentIsland) {
		this.currentIsland = currentIsland;
	}

	 public void valueIslandChanged(ValueChangeEvent event) {
	      /*  if (islandNameList.contains(event.getNewValue().toString())) 
	        	currentIsland = event.getNewValue().toString();
	        else if(typeTouristAttractionList.contains(event.getNewValue().toString()))
	        	currentType = event.getNewValue().toString();
	 		else if(choiceList.contains(event.getNewValue().toString()))
	 			currentChoice = event.getNewValue().toString();*/
	 		
	    }

	public List<SelectItem> getIslandList() {
		return islandList;
	}

	public void setIslandList(List<SelectItem> islandList) {
		this.islandList = islandList;
	}

	public String getCurrentType() {
		return currentType;
	}

	public void setCurrentType(String currentType) {
		this.currentType = currentType;
	}

	public String getCurrentChoice() {
		return currentChoice;
	}

	public void setCurrentChoice(String currentChoice) {
		this.currentChoice = currentChoice;
	}

	public List<SelectItem> getAttractionList() {
		return attractionList;
	}

	public void setAttractionList(List<SelectItem> attractionList) {
		this.attractionList = attractionList;
	}

	public List<SelectItem> getChoiceList() {
		return choiceList;
	}

	public void setChoiceList(List<SelectItem> choiceList) {
		this.choiceList = choiceList;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Date getSelectedArrivalDate() {
		return selectedArrivalDate;
	}

	public void setSelectedArrivalDate(Date selectedArrivalDate) {
		this.selectedArrivalDate = selectedArrivalDate;
	}

	public Date getSelectedDepartureDate() {
		return selectedDepartureDate;
	}

	public void setSelectedDepartureDate(Date selectedDepartureDate) {
		this.selectedDepartureDate = selectedDepartureDate;
	}
	
	public void buildOffer(){

		boolean choice;
		this.offerList=new ArrayList<Offer>();
		HashMap<Integer, Offer> hashmap = new HashMap<Integer, Offer>();
		
		if(currentChoice.equals("oui")){
			choice=true;
		}
		else{
			choice=false;
		}
		DateTime start= null;
		DateTime end =null;
		
		if(selectedArrivalDate != null && selectedDepartureDate != null){
			Pattern p1 = Pattern .compile("[A-z]+ ([A-z]+) ([0-9]+) 00:00:00 CEST ([0-9]+)");
			Pattern p2= Pattern .compile("[A-z]+ ([A-z]+) ([0-9]+) 00:00:00 CEST ([0-9]+)");
			Matcher m1 = p1.matcher(selectedArrivalDate.toString());
			Matcher m2 = p2.matcher(selectedDepartureDate.toString());
			if(m1.matches())start = new DateTime(Integer.parseInt(m1.group(3)),getTheMonth(m1.group(1)),Integer.parseInt(m1.group(2)),0,0,0, FRANCE);
			if(m2.matches())end = new DateTime(Integer.parseInt(m2.group(3)),getTheMonth(m2.group(1)),Integer.parseInt(m2.group(2)),0,0,0, FRANCE);
			System.out.println(selectedArrivalDate.toString() + " " + m1.group(1));
			System.out.println(selectedDepartureDate.toString() + " " + m2.group(1));
		}

		if(currentChoice != null && currentType != null && currentIsland !=null && currentType.length()>2 && currentIsland.length()>2 && currentChoice.length()>2){

		hashmap = createOffers.getOffersWithCriterias(start, end, choice, currentType, currentIsland, keyWord);

		for(int i = 1 ; i < hashmap.size()+1 ; i++){
			this.offerList.add(hashmap.get(i));
		}
		Tools.getInstance().setOffers((ArrayList<Offer>) offerList);
		}
	}

	public List<String> getCritereList() {
		return critereList;
	}

	public void setCritereList(List<String> critereList) {
		this.critereList = critereList;
	}

	public ORMBuilder getOrmBuilder() {
		return ormBuilder;
	}

	public void setOrmBuilder(ORMBuilder ormBuilder) {
		this.ormBuilder = ormBuilder;
	}

	public CreateOffers getCreateOffers() {
		return createOffers;
	}

	public void setCreateOffers(CreateOffers createOffers) {
		this.createOffers = createOffers;
	}

	public DateTimeZone getFRANCE() {
		return FRANCE;
	}

	public void setFRANCE(DateTimeZone fRANCE) {
		FRANCE = fRANCE;
	}

	public boolean isRowExcursionIsSelected() {
		return rowExcursionIsSelected;
	}

	public void setRowExcursionIsSelected(boolean rowExcursionIsSelected) {
		this.rowExcursionIsSelected = rowExcursionIsSelected;
	}

	public List<Travel> getTravelFromMap() {
		return travelFromMap;
	}

	public void setTravelFromMap(List<Travel> travelFromMap) {
		this.travelFromMap = travelFromMap;
	}
	
	public int getTravelListSize() {
	    return travelFromMap.size();
	}

	public List<Excursion> getExcursionListForOffer() {
		return excursionListForOffer;
	}

	public void setExcursionListForOffer(List<Excursion> excursionListForOffer) {
		this.excursionListForOffer = excursionListForOffer;
	}

	public String getSelectedExcursion() {
		return selectedExcursion;
	}

	public void setSelectedExcursion(String selectedExcursion) {
		this.selectedExcursion = selectedExcursion;
	}

	public Excursion getExcursion() {
		return excursion;
	}

	public void setExcursion(Excursion excursion) {
		this.excursion = excursion;
	}

	public String getIdTmpOffer() {
		return idTmpOffer;
	}

	public void setIdTmpOffer(String idTmpOffer) {
		this.idTmpOffer = idTmpOffer;
	}

	public TouristAttraction getCurrentTouristAttraction() {
		return currentTouristAttraction;
	}

	public void setCurrentTouristAttraction(TouristAttraction currentTouristAttraction) {
		this.currentTouristAttraction = currentTouristAttraction;
	}

	public List<String> autocompleteKeyWord(String prefix) {
        ArrayList<String> result = new ArrayList<String>();

            
                result.add("Plage");
                result.add("Randonnees");
                result.add("D�couverte");
                result.add("Plongee");
                result.add("D�tente");
                result.add("Facile");
                result.add("Volcan");
                result.add("Tropical");
                result.add("Cascade");
                result.add("Parc");
                result.add("Poisson");
           
 
        return result;
    }
	
	public boolean isActivateButtonOffer() {
		return activateButtonOffer;
	}

	public void setActivateButtonOffer(boolean activateButtonOffer) {
		this.activateButtonOffer = activateButtonOffer;
	}

	public Boolean getActivateBuildOfferButton(){
		
		if(currentIsland.equals("") ||  currentType.matches("") || currentChoice.isEmpty()){
			return true;
		}
		else{
			return false;
		}
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public List<String> getKeyWordList() {
		return keyWordList;
	}

	public void setKeyWordList(List<String> keyWordList) {
		this.keyWordList = keyWordList;
	}

	public List<String> getLatitudeList1() {
		return latitudeList1;
	}

	public void setLatitudeList1(List<String> latitudeList1) {
		this.latitudeList1 = latitudeList1;
	}

	public List<String> getLongitudeList1() {
		return longitudeList1;
	}

	public void setLongitudeList1(List<String> longitudeList1) {
		this.longitudeList1 = longitudeList1;
	}

	public List<String> getLatitudeList2() {
		return latitudeList2;
	}

	public void setLatitudeList2(List<String> latitudeList2) {
		this.latitudeList2 = latitudeList2;
	}

	public List<String> getLongitudeList2() {
		return longitudeList2;
	}

	public void setLongitudeList2(List<String> longitudeList2) {
		this.longitudeList2 = longitudeList2;
	}

	public List<Offer> getOfferListBis() {
		return offerListBis;
	}

	public void setOfferListBis(List<Offer> offerListBis) {
		this.offerListBis = offerListBis;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public int getTheMonth(String month){
		if(month != null){
			if(month.equals("Jan"))return 1;
			else if(month.equals("Feb"))return 2;
			else if(month.equals("Mar"))return 3;
			else if(month.equals("Apr"))return 4;
			else if(month.equals("May"))return 5;
			else if(month.equals("Jun"))return 6;
			else if(month.equals("Jul"))return 7;
			else if(month.equals("Aug"))return 8;
			else if(month.equals("Sep"))return 9;
			else if(month.equals("Oct"))return 10;
			else if(month.equals("Nov"))return 11;
			else if(month.equals("Dec"))return 12;
		}
		return 5;
			
	}
	
	public String getTheStringMonth(String month){
		if(month != null){
			if(month.equals("Jan"))return "Janvier";
			else if(month.equals("Feb"))return "F�vrier";
			else if(month.equals("Mar"))return "Mars";
			else if(month.equals("Apr"))return "Avril";
			else if(month.equals("May"))return "Mai";
			else if(month.equals("Jun"))return "Juin";
			else if(month.equals("Jul"))return "Juillet";
			else if(month.equals("Aug"))return "Aout";
			else if(month.equals("Sep"))return "Septempbre";
			else if(month.equals("Oct"))return "Octobre";
			else if(month.equals("Nov"))return "Novembre";
			else if(month.equals("Dec"))return "Decembre";
		}	
		
		return "Janvier";
	}
	
public String getTrueDate(org.richfaces.component.SequenceIterationStatus index, org.richfaces.component.SequenceIterationStatus index1) {
		
		offerList = Tools.getInstance().getOffers();
		
		String dayEnum = new String();
		String day = offerList.get(index.getIndex()).getDateDebut().plusDays(index1.getIndex()).toString().substring(8, 10);
		String month = offerList.get(index.getIndex()).getDateDebut().plusDays(index1.getIndex()).toString().substring(5, 7);
		month = getTheStringMonth(month);
		dayEnum=day.concat(" "+month);
		
		return dayEnum;
	} 
}
	