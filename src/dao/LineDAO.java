package dao;

import java.util.List;


import persistence.data.Line;



public interface LineDAO {

	void createLine(Line newLine);
	void dropLine(String typeTransport);
	List<Line> getAllTransport();
	Line searchTransportbyId(int idLine);
	Line searchTransportbyName(String typeTransport);
	Line searchTransportbySpeedAverage(double speedAverage);
	Line searchTransportbyPrice(double priceTransport);
}
