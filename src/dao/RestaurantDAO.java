package dao;

import java.util.List;

import persistence.data.Restaurant;

public interface RestaurantDAO {
	
	void createRestaurant(Restaurant newRestaurant);
	void dropRestaurant(String nameRestaurant);
	List<Restaurant> getAllRestaurants();
	Restaurant searchRestaurantsbyId(int idRestaurant);
	Restaurant searchRestaurantsbyName(String nameRestaurant);
	Restaurant searchRestaurantsbyType(String typeRestaurant);
	Restaurant searchRestaurantsbyStars(int stars);
}
