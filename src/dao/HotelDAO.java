package dao;

import java.util.List;

import persistence.data.Hotel;

public interface HotelDAO {
	 void createHotel(Hotel hotel);
	 void dropHotel();
	 List<Hotel> getAllHotels();
	 Hotel searchHotelbyId(int idHotel);
	 Hotel searchHotelbyName(String nameHotel);
	 Hotel searchHotelbyStars(int stars);

}
