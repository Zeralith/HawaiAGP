package dao;

import java.util.List;

import persistence.data.TouristAttraction;

public interface TouristAttractionDAO {
	
	void createTouristAttraction(TouristAttraction newTouristAttraction);
	void dropTouristAttraction();
	List<TouristAttraction> getAllTouristAttraction();
	TouristAttraction searchTouristAttractionbyId(int idAttraction);
	TouristAttraction searchTouristAttractionbyName(String nameAttraction);
	List<TouristAttraction> searchTouristAttractionbyType(String typeAttraction);

}
