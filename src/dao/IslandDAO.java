package dao;

import java.util.List;

import persistence.data.Hotel;
import persistence.data.Island;
import persistence.data.Restaurant;

public interface IslandDAO {
	 void createIsland(Island newIsland);
	 void dropIsland(String nameIsland);
	 List<Island> getAllIsland();
	 Island searchIslandbyId(int idIsland);
	 Island searchIslandbyName(String nameIsland);
	 List<Restaurant> getRestaurantsbyIsland(String nameIsland);
	 List<Hotel> getHotelbyIsland(String nameIsland);
	 Island searchIslandbyRestaurant(int idRestaurant);
	 Island searchIslandbyHub(int idHub);
	 Island searchIslandByHotel(int idHotel);

}
