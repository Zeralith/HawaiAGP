package dao;

import java.util.List;

import persistence.data.Hub;

public interface HubDAO {
	 void createHub(Hub hub);
	 void dropHub();
	 List<Hub> getAllHub();
	 Hub searchHubbyId(int id);
	 Hub searchHubbyType(String nameHub);
	 List<Hub> searchHubByLine(int idLine);

}
