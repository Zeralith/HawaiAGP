package lucene;

public class LuceneDocInst 
{
	private int id;
	private String textdata;
	
	public LuceneDocInst(int id, String textdata) {
		super();
		this.id = id;
		this.textdata = textdata;
	}	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTextdata() {
		return textdata;
	}

	public void setTextdata(String textdata) {
		this.textdata = textdata;
	}

}
