package lucene;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

abstract public class LuceneDocument 
{	
	static private String DIR_SEPARATOR = "/";
	static private String EXT_FILTER = ".txt";
	static private String indexDir = "build/lucene/index";
	static private String dataDir = "build/lucene/data";
	static private Indexer indexer;
	static private Searcher searcher;

	private int nbfiles;

	private int nbresult;
	private List<Integer> resultid;
	private Iterator<Integer> iterid;
	private long searchduration;

	public final String getIndexDir()
	{
		return indexDir+DIR_SEPARATOR+this.getEntityName();
	}

	public final String getDataDir()
	{
		return dataDir+DIR_SEPARATOR+this.getEntityName();
	}
	
	abstract public String getEntityName(); // precise le sous-r�pertoire pour l'entit� concern�e

	public LuceneDocument()
	{
		super();
		this.nbfiles = 0;
		this.nbresult = 0;
		this.searchduration = -1;
	}
	
	public final int init(String querystring) throws IOException, ParseException
	{
		if(this.count() == 0)
			this.create();	// create doc and index if needed

		this.search(querystring);
		
		return this.nbresult;
	}
	
	public final boolean hasNext()
	{
		return ((this.nbresult > 0) && (this.iterid.hasNext()));
	}
	
	public final int next()
	{
		return this.iterid.next();
	}
	
	abstract protected List<LuceneDocInst> generateInstances();

	public final void create() throws IOException
	{
		List<LuceneDocInst> listDocInstances = generateInstances();	
		Iterator<LuceneDocInst> iterDocInst = listDocInstances.iterator();
		
		while(iterDocInst.hasNext())
		{
			LuceneDocInst di = iterDocInst.next();
			
			FileWriter fichier = new FileWriter(getDataDir()+DIR_SEPARATOR+di.getId()+EXT_FILTER);
			fichier.write (di.getTextdata());
			fichier.close();
		}
		
		nbfiles = createIndex();
		
		FileWriter fichier = new FileWriter(getDataDir()+EXT_FILTER);
		
		fichier.write(""+nbfiles);
		fichier.close();
	}

	private int createIndex() throws IOException
	{
		indexer = new Indexer(getIndexDir());
		int numIndexed = 0;
		numIndexed = indexer.createIndex(getDataDir(), new TextFileFilter(EXT_FILTER));
		indexer.close();
		
		return numIndexed;
	}

	private void search(String searchQuery) throws IOException, ParseException
	{
		String idtxt;
		int id;
		this.nbresult = 0;
		this.resultid = new ArrayList<Integer>();

		this.searchduration = System.currentTimeMillis();

		searcher = new Searcher(getIndexDir());
		TopDocs hits = searcher.search(searchQuery,this.count());

		for(ScoreDoc scoreDoc : hits.scoreDocs) 
		{
			Document doc = searcher.getDocument(scoreDoc);
			idtxt = doc.get(LuceneConstants.FILE_NAME);
			
			id = new Integer(idtxt.trim().replace(EXT_FILTER, ""));

			if(id > 0)
			{
				this.resultid.add(id);
//				System.out.println("File: " + doc.get(LuceneConstants.FILE_PATH));
				this.nbresult++;
			}
		}
		searcher.close();
		this.searchduration = System.currentTimeMillis() - this.searchduration;
		
		System.out.println(hits.totalHits +
				" documents found. Time Taken : " + (this.searchduration) + " ms");

		this.iterid = this.resultid.iterator();
	}
	
	public final int count() 
	{

		if(nbfiles == 0)
		{
			try	// update files count if any
			{
				//Creates a FileReader Object
				FileReader fr = new FileReader(getDataDir()+EXT_FILTER);
			
				char [] a = new char[50];
				fr.read(a); // reads the content to the array
				fr.close();
				
				String txtread = "";
				
				for( char c : a )
					txtread = txtread+c;
				
				nbfiles = new Integer(txtread.trim());
			}
			catch(IOException ioe)	// file doesn't exist
			{
				return 0;
			}

		}	
		
		return nbfiles;
	}
	

}
