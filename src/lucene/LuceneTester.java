package lucene;

import java.io.IOException;
import org.apache.lucene.queryParser.ParseException;

import lucene.data.TouristAttractionLucene;

public class LuceneTester 
{

	public static void main(String[] args) 
	{
		TouristAttractionLucene tal = new TouristAttractionLucene();
		
		try 
		{
			String srch = "volcan Big Island";
			int n = tal.init(srch);
			System.out.println("Found "+n+" documents for << "+srch+" >>");
			
			while(tal.hasNext())
				System.out.println("id = "+tal.next());
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}