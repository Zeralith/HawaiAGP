package lucene.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lucene.LuceneDocInst;
import lucene.LuceneDocument;
import persistence.data.Island;
import persistence.data.TouristAttraction;
import persistence.hibernate.IslandDAOHibernate;

public class TouristAttractionLucene extends LuceneDocument 
{
	public String getEntityName()
	{	// need a "TouristAttraction" named folder in dataDir and in indexDir.  
		return "TouristAttraction";
	}

	protected List<LuceneDocInst> generateInstances() 
	{
		String ln = String.format("%n");
		List<LuceneDocInst> listDocInst = new ArrayList<LuceneDocInst>();
		
		IslandDAOHibernate islandDAO = new IslandDAOHibernate();
		List<Island> islands = islandDAO.getAllIsland();
		Iterator<Island> iterIsland = islands.iterator();
		
		while(iterIsland.hasNext())
		{
			String island = iterIsland.next().getNameIsland();
			
			List<TouristAttraction> listTA = islandDAO.getTouristAttractionsbyIsland(island);
			Iterator<TouristAttraction> iterTA = listTA.iterator();

			while(iterTA.hasNext())
			{
				TouristAttraction ta = iterTA.next();
				
				String datastring = new String(	
						ta.getTypeAttraction()	+	ln
					+	ta.getNameAttraction()	+	ln
					+	ta.getDescription()		+	ln
					+	island					+	ln
				);

				listDocInst.add(new LuceneDocInst(ta.getIdAttraction(), datastring));
			}
			
		}
		
		return listDocInst;
	}

}
