package lucene;

import java.io.File;
import java.io.FileFilter;

public class TextFileFilter implements FileFilter 
{
	private String filter;
	
	public TextFileFilter(String filter) {
		super();
		this.filter = filter;
	}

	public boolean accept(File pathname)
	{
		return pathname.getName().toLowerCase().endsWith(filter);
	}
}