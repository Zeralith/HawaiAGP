package test;

import test.buisness.dijkstra.GraphTest;
import test.buisness.engine.ManageTravelTest;
import test.dao.RestaurantDAOTest;
import test.dao.TouristAttractionDAOTest;

public class ExecuteAllJunitTest {
	public static void main(String args[]){
		junit.textui.TestRunner.run(GraphTest.class);
		junit.textui.TestRunner.run(ManageTravelTest.class);
		junit.textui.TestRunner.run(RestaurantDAOTest.class);
		junit.textui.TestRunner.run(TouristAttractionDAOTest.class);
	}

}