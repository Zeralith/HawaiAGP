package test.operator;

import junit.framework.TestCase;
import operator.OperatorLuceneSQL;

public class OperatorLuceneSqlTest extends TestCase {
	
	public static String query = "select touristAttraction.idAttraction,touristAttraction.nameAttraction from TouristAttraction as touristAttraction with volcan visite";
	
	public static void main(String[] args){
		junit.textui.TestRunner.run(OperatorLuceneSqlTest.class);
	}
	public void testOperator(){
		boolean loop = true;
		OperatorLuceneSQL operator = new OperatorLuceneSQL();
		operator.init(query);
		while(loop)
		{
			if(operator.next() == null)
				loop = false;
		}
		
	}

}
