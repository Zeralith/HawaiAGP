package test.operator;

import java.util.ArrayList;

import buisness.model.TouristAttraction;
import junit.framework.TestCase;
import operator.OperatorMixt;
import persistence.hibernate.IslandDAOHibernate;
import persistence.hibernate.TouristAttractionDAOHibernate;

public class OperatorMixtTest extends TestCase {

	public static String query = " ";
	
	public static void main(String[] args)
	{	
		junit.textui.TestRunner.run(OperatorMixtTest.class);
	}
	
	public void testOperator()
	{
		boolean loop = true;
		OperatorMixt operator = new OperatorMixt();
		operator.init("select touristAttraction.idAttraction,touristAttraction.nameAttraction from TouristAttraction as touristAttraction with volcan visite");
		while(loop)
		{
			if(operator.next() == null)
				loop = false;
		}
		
	}
	
	public void testdemerde(){
		boolean loop = true;
		OperatorMixt operator = new OperatorMixt();
		String lucene = "volcan";
		String query = "select ta.idAttraction,ta.nameAttraction from TouristAttraction as ta with "+lucene;
		operator.init(query);
		System.out.println();
		Integer id;
		ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();
		
		while(loop){
			
			Object obj = operator.next();
			
			Object data = (Object) obj;
			
			if(obj == null){
				loop = false;	
			} else {
				id = (Integer) data;
				persistence.data.TouristAttraction taPersist = TouristAttractionDAOHibernate.getInstance().searchTouristAttractionbyId(id);
				touristAttractions.add(new TouristAttraction(taPersist.getNameAttraction(),
						taPersist.getTypeAttraction(),
						taPersist.getDescription(),
						taPersist.getPhotolinkAttraction(),
						taPersist.getDurationVisit(),
						taPersist.getCoordGPS(),
						islandDAO.searchIslandbyTouristAttraction(taPersist.getIdAttraction()).getNameIsland(),
						taPersist.getPriceAttraction()));
			}
		}
		
		System.out.println(touristAttractions);
	}

}
