package test.operator;

import junit.framework.TestCase;
import test.operator.OperatorLuceneTest;
import test.operator.OperatorLuceneSqlTest;
import test.operator.OperatorSqlTest;
import test.operator.OperatorMixtTest;

public class OperatorMultiTest extends TestCase 
{

	public static void main(String[] args)
	{
		// make initialization here
		OperatorSqlTest.query = "select idAttraction from TouristAttraction";
		OperatorLuceneTest.query = "TouristAttraction";
		junit.textui.TestRunner.run(OperatorSqlTest.class);
		junit.textui.TestRunner.run(OperatorLuceneTest.class);
		
		// Now we can begin without initialization latence.
		System.out.println("");
		System.out.println("-------------------------------------------------------");
		System.out.println("----------------The tests begin now !!!----------------");
		System.out.println("-------------------------------------------------------");

		System.out.println("");
		System.out.println("-------------Test time if no result found.-------------");
		System.out.println("");

		OperatorSqlTest.query = "select ta.idAttraction, ta.nameAttraction from TouristAttraction as ta where nameAttraction = 'blablabla'";
		OperatorLuceneTest.query = "blablabla";
		OperatorLuceneSqlTest.query = OperatorSqlTest.query + " with " + OperatorLuceneTest.query;
		OperatorMixtTest.query = OperatorLuceneSqlTest.query;
		System.out.println("Query : << "+OperatorMixtTest.query+" >>");

		junit.textui.TestRunner.run(OperatorMixtTest.class);
		junit.textui.TestRunner.run(OperatorLuceneSqlTest.class);
		
		System.out.println("");
		System.out.println("-------------Test time with few criteria-------------");
		System.out.println("");
		
		System.out.println("We want visit time less than 5 hours for a volcan on the Oahu Island.");
		OperatorSqlTest.query = "select ta.idAttraction, ta.nameAttraction from TouristAttraction as ta where durationVisit <= 5";
		OperatorLuceneTest.query = "volcan Oahu";
		OperatorLuceneSqlTest.query = OperatorSqlTest.query + " with " + OperatorLuceneTest.query;
		OperatorMixtTest.query = OperatorLuceneSqlTest.query;

		System.out.println("Query : << "+OperatorMixtTest.query+" >>");
		junit.textui.TestRunner.run(OperatorMixtTest.class);
		junit.textui.TestRunner.run(OperatorLuceneSqlTest.class);


		System.out.println("");
		System.out.println("-------------Test time with few criteria-------------");
		System.out.println("");
		
		System.out.println("We want to discover Hawaiian food for less than 250 euros.");
		OperatorSqlTest.query = "select ta.idAttraction, ta.nameAttraction from TouristAttraction as ta where priceAttraction <= 250";
		OperatorLuceneTest.query = "food restaurant feed manger nourriture repas";
		OperatorLuceneSqlTest.query = OperatorSqlTest.query + " with " + OperatorLuceneTest.query;
		OperatorMixtTest.query = OperatorLuceneSqlTest.query;

		System.out.println("Query : << "+OperatorMixtTest.query+" >>");
		junit.textui.TestRunner.run(OperatorMixtTest.class);
		junit.textui.TestRunner.run(OperatorLuceneSqlTest.class);


	}

}
