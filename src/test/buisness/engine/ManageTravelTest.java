package test.buisness.engine;

import java.util.ArrayList;
import java.util.HashMap;

import buisness.engine.BuildExcursion;
import buisness.engine.ManageTravel;
import buisness.model.Excursion;
import buisness.model.Hotel;
import buisness.model.Hub;
import buisness.model.Line;
import buisness.model.TouristAttraction;
import buisness.model.Travel;
import junit.framework.TestCase;

public class ManageTravelTest extends TestCase {
	private ManageTravel manageTravel = null;
	private ArrayList<Line> lines = null;
	
	public static void main(String[] args){
		junit.swingui.TestRunner.run(ManageTravelTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
        ArrayList<Hub> hubs1 = new ArrayList<Hub>();
        ArrayList<Hub> hubs2 = new ArrayList<Hub>();
        ArrayList<Hub> hubs1A = new ArrayList<Hub>();
        lines = new ArrayList<Line>();
        hubs1A.add(new Hub("19.638968, -155.994291","island1","hubA1"));
        hubs1A.add(new Hub("20.743927, -156.457305","island2","hubA2"));
        hubs1A.add(new Hub("21.290135, -157.852304","island3","hubA3"));
        
        hubs1.add(new Hub("48.991743, 1.853170", "island1", "hubA"));
        hubs1.add(new Hub("49.022610, 1.874773", "island1", "hubB"));
        hubs2.add(new Hub("48.997412, 1.853734", "island1", "hubC"));
        hubs2.add(new Hub("49.019284, 1.926807", "island1", "hubD"));
        lines.add(new Line("Train", 2.0, "Lignea", hubs1, 80.0));
        lines.add(new Line("Train", 2.0, "Ligneb", hubs2, 80.0));
        lines.add(new Line("Ship", 1.0, "LigneCShip", hubs1A, 30.0));
        manageTravel = ManageTravel.getInstance();
  }

  protected void tearDown() throws Exception {
        super.tearDown();
        manageTravel = null;
  }
  
  public void testTravelHaSaSb(){
	  HashMap<Integer, Travel>travelsHashMap = new HashMap<Integer, Travel>();
	  Travel travel;
	  double time = 0.0;
	  TouristAttraction touristAttractionA = new TouristAttraction("sA", "124",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "48.995118, 1.847075", "island1",1);
	  TouristAttraction touristAttractionB = new TouristAttraction("sB", "123",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "49.020084, 1.923143", "island1",1);
	  Hotel hotel = new Hotel("Poupidou", "domingo", 40.0, "49.028707, 1.756522", "island1",1);
	  
	  manageTravel.setTouristAttractionA(touristAttractionA);
	  manageTravel.setTouristAttractionB(null);
	  manageTravel.setHotelB(hotel);
	  travel = manageTravel.travelBetweenDijkstra(hotel.getGpsCoordinatesHotel(),
			  touristAttractionA.getGpsCoordinatesTouristAttraction(), hotel.getIslandName(),
			  touristAttractionA.getIslandName(), true);
	  time += travel.getTravelTime();
	  travelsHashMap.put(0, travel);
	  
	  travel = null;
	  manageTravel.setTouristAttractionA(touristAttractionA);
	  manageTravel.setTouristAttractionB(touristAttractionB);
	  manageTravel.setHotelB(null);
	  travel = manageTravel.travelBetweenDijkstra(touristAttractionA.getGpsCoordinatesTouristAttraction(),
			  touristAttractionB.getGpsCoordinatesTouristAttraction(), touristAttractionA.getIslandName(),
			  touristAttractionA.getIslandName(), false);
	  time += travel.getTravelTime();
	  travelsHashMap.put(1, travel);
	  
	  travel = null;
	  manageTravel.setTouristAttractionA(touristAttractionB);
	  manageTravel.setTouristAttractionB(null);
	  manageTravel.setHotelB(hotel);
	  travel = manageTravel.travelBetweenDijkstra(touristAttractionB.getGpsCoordinatesTouristAttraction(),
			  hotel.getGpsCoordinatesHotel(), touristAttractionB.getIslandName(),
			  hotel.getIslandName(), false);
	  time += travel.getTravelTime();
	  travelsHashMap.put(2, travel);
	  
	  Excursion excursion = new Excursion(travelsHashMap, null, time);
	  System.out.println(excursion);
  }
  
  public void testExcursionHaSaSb(){
	  TouristAttraction touristAttractionA = new TouristAttraction("sA", "124",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "48.995118, 1.847075", "island1",1);
	  TouristAttraction touristAttractionB = new TouristAttraction("sB", "123",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "49.020084, 1.923143", "island1",1);
	  Hotel hotel = new Hotel("Poupidou", "domingo", 40.0, "49.028707, 1.756522", "island1",1);
	  
	  ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
	  touristAttractions.add(touristAttractionA);
	  touristAttractions.add(touristAttractionB);
	  
	  Excursion excursion = BuildExcursion.getInstance().createExcursion(touristAttractions, hotel);
	  System.out.println(excursion);
  }
  
  public void testTravelSaSb(){
	  Travel travel;
	  TouristAttraction touristAttractionA = new TouristAttraction("sA", "124",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "48.995118, 1.847075", "island1",1);
	  TouristAttraction touristAttractionB = new TouristAttraction("sB", "123",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "49.020084, 1.923143", "island1",1);
	  manageTravel.setTouristAttractionA(touristAttractionA);
	  manageTravel.setTouristAttractionB(touristAttractionB);
	  travel = manageTravel.travelBetweenDijkstra(touristAttractionA.getGpsCoordinatesTouristAttraction(),
			  touristAttractionB.getGpsCoordinatesTouristAttraction(), touristAttractionA.getIslandName(),
			  touristAttractionB.getIslandName(), false);
	  System.out.println(travel);
  }
  
  public void testTravelSaSbDijkstra(){
	  Travel travel;
	  TouristAttraction touristAttractionA = new TouristAttraction("sA", "124",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "48.995118, 1.847075", "island1",1);
	  TouristAttraction touristAttractionB = new TouristAttraction("sB", "123",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "49.020084, 1.923143", "island1",1);
	  manageTravel.setTouristAttractionA(touristAttractionA);
	  manageTravel.setTouristAttractionB(touristAttractionB);
	  travel = manageTravel.travelBetweenDijkstra(touristAttractionA.getGpsCoordinatesTouristAttraction(),
			  touristAttractionB.getGpsCoordinatesTouristAttraction(), touristAttractionA.getIslandName(),
			  touristAttractionB.getIslandName(), false);
	  System.out.println(travel);
  }
  
  public void testTravelSaSbDijkstraDifferentIsland(){
	  Travel travel;
	  TouristAttraction touristAttractionA = new TouristAttraction("sA", "124",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "19.472185, -155.840591", "island1",1);
	  TouristAttraction touristAttractionB = new TouristAttraction("sB", "123",
			  "Loisir", "Lopirum Dslin", 1.5,
			  "21.314584, -157.855637", "island3",1);
	  manageTravel.setTouristAttractionA(touristAttractionA);
	  manageTravel.setTouristAttractionB(touristAttractionB);
	  travel = manageTravel.travelBetweenDijkstra(touristAttractionA.getGpsCoordinatesTouristAttraction(),
			  touristAttractionB.getGpsCoordinatesTouristAttraction(), touristAttractionA.getIslandName(),
			  touristAttractionB.getIslandName(), false);
	  System.out.println(travel);
  }

  public void testGps() {
	  String gps1 = "19.2040682,-155.5961578";
	  String gps2 = "19.442251,-155.2376877";
	  double d = manageTravel.distanceBetweenGps(gps1, gps2);
	  
	  System.out.println("Distance :: " + d);
	  //assertEquals("La distance n est pas correcte ",15.632798341497436, d);
  }
  
}
