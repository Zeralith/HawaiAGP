package test.buisness.engine;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;

import buisness.engine.CreateOffers;
import buisness.model.Offer;
import junit.framework.TestCase;

public class CreateOffersTest extends TestCase {
	
	
	public static void main(String[] args){
		junit.textui.TestRunner.run(CreateOffersTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
	}

	protected void tearDown() throws Exception {
        super.tearDown();
	}
	
	public void testNbDays(){
		DateTimeZone FRANCE = DateTimeZone.forID("Europe/Paris");
		DateTime start = new DateTime(2016, 10, 20, 5, 0, 0, FRANCE);
		DateTime end = new DateTime(2016, 10, 26, 13, 0, 0, FRANCE);
		int nbDay = Days.daysBetween(start.withTimeAtStartOfDay(),
                end.withTimeAtStartOfDay()).getDays();
		assertEquals(6, nbDay);
	}
	
	public void testCreateOffers(){
		CreateOffers createOffers = CreateOffers.getInstance();
		
		HashMap<Integer, Offer> offers = createOffers.getOffersWithCriterias(null, null, false, "Loisir", "ile Hawaii", null);
		for(Integer idOffer : offers.keySet()){
			for(Integer idExc : offers.get(idOffer).getExcursionsHashMap().keySet()){
				System.out.println(offers.get(idOffer).getExcursionsHashMap().get(idExc).getLunchRestaurant().toString());
			}
			System.out.println("nb ex : " + offers.get(idOffer).getExcursionsHashMap().size());
		}
	}
}
