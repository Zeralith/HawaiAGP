package test.buisness.dijkstra;

import java.util.HashMap;

import buisness.dijkstra.Graph;
import buisness.dijkstra.Graph.Vertex;
import junit.framework.TestCase;

public class GraphTest extends TestCase{
	
	public static void main(String[] args){
		junit.swingui.TestRunner.run(GraphTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
	}
	
	protected void tearDown() throws Exception {
        super.tearDown();
	}
	
	public void testDijkstra(){
		Graph.Edge[] GRAPH = {
			      new Graph.Edge("a", "b", 7, 0.0),
			      new Graph.Edge("a", "c", 9, 0.0),
			      new Graph.Edge("a", "f", 14, 0.0),
			      new Graph.Edge("b", "c", 10, 0.0),
			      new Graph.Edge("b", "d", 15, 0.0),
			      new Graph.Edge("c", "d", 11, 0.0),
			      new Graph.Edge("c", "f", 2, 0.0),
			      new Graph.Edge("d", "e", 6, 0.0),
			      new Graph.Edge("e", "f", 9, 0.0),
			   };
		
		Graph g = new Graph(GRAPH);
		Vertex v;
		g.dijkstra("a");
		g.printPath("d");
		HashMap<Integer, Vertex> track = new HashMap<Integer, Graph.Vertex>();
		int key = 0;
		v = g.getGraph().get("d");
		while(v.previous != null && v != v.previous){
			track.put(key, v);
			key++;
			v = v.previous;
		}
		track.put(key, g.getGraph().get("a"));
		
		//Read in reverse track = best track
		for(int i=track.size()-1; i>-1 ; i--){
			System.out.print(track.get(i).name + " -> ");
		}
		//a -> c(9,00) -> d(20,00)
	}
	
}
