package test.database;

import dao.LineDAO;
import persistence.data.Line;
import persistence.hibernate.LineDAOHibernate;


public class LineLoadData {

	public static Line loadAirLine(){
		LineDAO lineDAO = new LineDAOHibernate();
		
		Line line1 = new Line (1,"Mokulele Airlines","Airplane", 0.9, 300.0);// en euros
		
		lineDAO.createLine(line1);
		
		return line1;
	}

	public static Line loadSeaLine(){
		LineDAO lineDAO = new LineDAOHibernate();
		
		Line line2 = new Line (2,"Sail Blue Hawaii","Boat",0.5,30.0);
		
		lineDAO.createLine(line2);
		
		return line2;
	}
}
