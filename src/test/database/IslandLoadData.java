package test.database;


import java.util.ArrayList;

import persistence.data.Hotel;
import persistence.data.Island;
import persistence.data.Restaurant;
import persistence.data.TouristAttraction;
import persistence.hibernate.IslandDAOHibernate;


public class IslandLoadData {
	
	public static Island loadIsland1(ArrayList<TouristAttraction> touristAttraction,ArrayList<Hotel> hotel ,ArrayList<Restaurant> restaurant){ 
		
		IslandDAOHibernate islandDAO = new IslandDAOHibernate();
		Island island1 = new Island(1,"ile Hawaii",touristAttraction,hotel,restaurant);
		islandDAO.createIsland(island1);
		return island1;
	}
	
	public static Island loadIsland2(ArrayList<TouristAttraction> touristAttraction,ArrayList<Hotel> hotel,ArrayList<Restaurant> restaurant){ 
		IslandDAOHibernate islandDAO = new IslandDAOHibernate();
		Island island2 = new Island(2,"ile Maui",touristAttraction,hotel,restaurant);
		islandDAO.createIsland(island2);

		return island2;
		
	} 
	public static Island loadIsland3(ArrayList<TouristAttraction> touristAttraction,ArrayList<Hotel> hotel,ArrayList<Restaurant> restaurant){ 
	
		IslandDAOHibernate islandDAO = new IslandDAOHibernate();
		Island island3 = new Island(3,"ile Oahu",touristAttraction,hotel,restaurant);
		islandDAO.createIsland(island3);
		return island3;

	}
	public static Island loadIsland4(ArrayList<TouristAttraction> touristAttraction,ArrayList<Hotel> hotel,ArrayList<Restaurant> restaurant){ 
	
		IslandDAOHibernate islandDAO = new IslandDAOHibernate();
		Island island4 = new Island(4,"ile Kauai",touristAttraction,hotel,restaurant);
		islandDAO.createIsland(island4);
		return island4;
	}
	public static Island loadIsland5(ArrayList<TouristAttraction> touristAttraction,ArrayList<Hotel> hotel,ArrayList<Restaurant> restaurant){ 
		IslandDAOHibernate islandDAO = new IslandDAOHibernate();
		Island island5 = new Island(5,"ile Molokai",touristAttraction,hotel,restaurant);
		islandDAO.createIsland(island5);
		return island5;
	}

}
