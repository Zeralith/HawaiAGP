package test.database;

import java.util.ArrayList;

import dao.HubDAO;
import persistence.data.Hub;
import persistence.data.Island;
import persistence.data.Line;
import persistence.hibernate.HubDAOHibernate;


public class HubLoadData 
{
	static private int id = 1;
	
	public static ArrayList<Hub> loadPort1(Line sealine,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();

		hubs.add(new Hub(sealine,island,id++,"Hilo Harbor","19.730790, -155.053632"));
		hubs.add(new Hub(sealine,island,id++,"Anaehoomalu Bay","19.918363, -155.888641"));
		hubs.add(new Hub(sealine,island,id++,"Honokohau Harbor","19.670230, -156.023520"));
		hubs.add(new Hub(sealine,island,id++,"Kailua Pier","19.640103, -155.996947"));
		hubs.add(new Hub(sealine,island,id++,"Kawaihae Harbor","20.039363, -155.831029"));
		hubs.add(new Hub(sealine,island,id++,"Keahou Harbor","19.560930, -155.962413"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));
			
		return hubs;
	}

	public static ArrayList<Hub> loadPort2(Line sealine,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();

		hubs.add(new Hub(sealine,island,id++,"Lahaina Harbor","20.885113, -156.686640"));
		hubs.add(new Hub(sealine,island,id++,"Kahului Harbor","20.898379, -156.481081"));
		hubs.add(new Hub(sealine,island,id++,"Hana Harbor","20.756184, -155.982079"));
		hubs.add(new Hub(sealine,island,id++,"Maalaea Harbor","20.792070, -156.511705"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));
		
		return hubs;
	}

	public static ArrayList<Hub> loadPort3(Line sealine,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();

		hubs.add(new Hub(sealine,island,id++,"Honolulu Harbor","21.309809, -157.878992"));
		hubs.add(new Hub(sealine,island,id++,"Kewalo Basin","21.294091, -157.855922"));
		hubs.add(new Hub(sealine,island,id++,"Ala Wai Harbor","21.283917, -157.840486"));
		hubs.add(new Hub(sealine,island,id++,"Pearl Harbor","21.352976, -157.949274"));
		hubs.add(new Hub(sealine,island,id++,"Kalaeloa Barbers Point Harbor","21.320489, -158.117432"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));
		
		return hubs;
	}
	public static ArrayList<Hub> loadPort4(Line sealine,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();

		hubs.add(new Hub(sealine,island,id++,"Nawiliwili Harbor","21.954929, -159.356130"));
		hubs.add(new Hub(sealine,island,id++,"Port Allen Harbor","21.900122, -159.587835"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));
		
		return hubs;
	}
	public static ArrayList<Hub> loadPort5(Line sealine,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();

		hubs.add(new Hub(sealine,island,id++,"Kaunakaikai Harbor","21.085942, -157.023751"));
		hubs.add(new Hub(sealine,island,id++,"Hale O Lono Harbor","21.087088, -157.247980"));

		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));
		
		return hubs;
	}

	public static ArrayList<Hub> loadAirport1(Line airline,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();
			
		hubs.add(new Hub(airline,island,id++,"Waimea-Kohala Airport","20.0008224,-155.672354"));
		hubs.add(new Hub(airline,island,id++,"Upper Paauau Airport","19.2040682,-155.5961578"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));

		return hubs;
	}

	public static ArrayList<Hub> loadAirport2(Line airline,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();
			
		hubs.add(new Hub(airline,island,id++,"A�roport de Kahului","20.8774618,-156.453778"));
		hubs.add(new Hub(airline,island,id++,"Hana Airport","20.8055895,-156.2409179"));			
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));

		return hubs;
	}

	public static ArrayList<Hub> loadAirport3(Line airline,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();

		hubs.add(new Hub(airline,island,id++,"Kalaeloa Airport","21.325067,-158.0337626"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));

		return hubs;
	}

	public static ArrayList<Hub> loadAirport4(Line airline,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();
			
		hubs.add(new Hub(airline,island,id++,"HI01","22.1482445,-159.4789833"));
		hubs.add(new Hub(airline,island,id++,"Hanamaulu Airstrip","22.0028251,-159.4789911"));
		hubs.add(new Hub(airline,island,id++,"Barking Sands Airport","22.0044167,-159.6712519"));
		hubs.add(new Hub(airline,island,id++,"Port Allen AirportPort Allen Airport","21.9194016,-159.5874811"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));

		return hubs;
	}

	public static ArrayList<Hub> loadAirport5(Line airline,Island island)
	{
		HubDAO hubDAO = new HubDAOHibernate();
		ArrayList<Hub> hubs = new ArrayList<Hub>();
			
		hubs.add(new Hub(airline,island,id++,"Brandt Field","21.1096982,-157.0080871"));
		hubs.add(new Hub(airline,island,id++,"Molokai Airport","21.1096982,-157.0080871"));
			
		for(int i = 0 ; i < hubs.size() ; i++)
			hubDAO.createHub(hubs.get(i));

		return hubs;
	}
}
