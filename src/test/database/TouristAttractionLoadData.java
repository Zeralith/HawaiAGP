package test.database;

import java.util.ArrayList;

import dao.TouristAttractionDAO;

import persistence.data.TouristAttraction;
import persistence.hibernate.TouristAttractionDAOHibernate;


public class TouristAttractionLoadData {

	public static ArrayList<TouristAttraction> touristAttractionLoad1(){
		
		TouristAttractionDAO TouristAttractionDAO = new TouristAttractionDAOHibernate();
		ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		
		/*Hawai Island*/
		TouristAttraction touristAttraction1 = new TouristAttraction(1,"Aventure aux volcans hawa�ens de Big Island","/images/ile1_hawaii/touristAttraction/touristAttraction1.jpg","Historique",92.79,"Admirez les champs de lave au cours d'une excursion compl�te d'une journ�e enti�re aux volcans de Big Island, � Hawa�. Vous explorerez le parc national des volcans, le volcan de Kilauea et les coul�es de lave et les crat�res de Big Island. Pendant le trajet, vous d�couvrirez l'histoire naturelle de cette �tonnante partie du monde.",
				"19.4068195,-155.2847598",9.5);
		TouristAttraction touristAttraction2 = new TouristAttraction(2,"Circuit en tout-terrain sur Big Island dans Waipio Valley","/images/ile1_hawaii/touristAttraction/touristAttraction2.jpg","Loisir",150.29,"Partez � la d�couverte de la luxuriante Waipio Valley de Big Island lors de cette aventure en tout-terrain id�ale pour toute la famille. Conduisez votre propre tout-terrain ou laissez-vous conduire par un guide et admirez le paysage tropical regorgeant de cascades, de falaises imposantes, de paysages verdoyants et de plages de sable noir. Vous ferez un arr�t pour nager dans le bassin d'une cascade, profiterez de vues magnifiques sur la c�l�bre cascade Hiilawe, l'une des plus hautes d'Hawa�, et profiterez de rafra�chissements l�gers. Aucune exp�rience n'est n�cessaire, et votre guide exp�riment� vous assurera un moment agr�able en toute s�curit�.",
				"20.1178404,-155.5774954",3.5);
		TouristAttraction touristAttraction3 = new TouristAttraction(3,"Luau L�gendes et h�ritages des volcans sur Big Island","/images/ile1_hawaii/touristAttraction/touristAttraction3.jpg","Historique",79.86,"Laissez vous entra�ner au c�ur des l�gendes et traditions du Pacifique Sud, et profitez d'une soir�e festive, musicale et dansante � ce luau de la plus grande de toutes les �les de l�Archipel. Le merveilleux coucher de soleil de la c�te de Kona est la toile de fond id�ale des rythmes polyn�siens lancinants du luau des l�gendes et h�ritages des volcans.",
				"19.632069,-155.9925247",3.0);
		TouristAttraction touristAttraction4 = new TouristAttraction(4,"Hilo Hot Steam Volcano Tour","/images/ile1_hawaii/touristAttraction/touristAttraction4.jpeg","Historique",140.85,"The Small-Group Hilo Hot Steam Volcano Tour is the one of the best and easiest ways to explore the highlights of Hilo and Hawaii Volcanoes National Park together with a knowledgeable interpretive guide.",
				"19.719283,-155.1181768",6.5);
		TouristAttraction touristAttraction5 = new TouristAttraction(5,"Big Island en une journ�e�: visite en petit groupe des volcans, des cascades et des sites touristiques et historiques","/images/ile1_hawaii/touristAttraction/touristAttraction5.jpg","Historique",155.97,"Cette visite de 10 heures en pleine nature offre un aper�u dans les moindres d�tails de l'�le la plus r�cente et la plus vaste de Hawa�. Explorez la diversit� du paysage et les merveilles naturelles, notamment les volcans actifs du parc national des volcans d'Hawa�, o� des coul�es de lave orange se jettent dans la mer. Traversez les for�ts tropicales jusqu'aux cascades et promenez-vous sur les plages de sable noir tout en balayant l'horizon du regard � la recherche de tortues vertes. Cette visite id�ale pour les familles implique une activit� mod�r�e de marche et pr�sente l'�le aux personnes de tous �ges. Elle comprend un d�jeuner, des encas, des rafra�chissements et le prix d'entr�e au parc.",
				"19.7611759,-155.9852374",10.0);
		TouristAttraction touristAttraction6 = new TouristAttraction(6,"Visite en petit groupe � Big Island : Parc national des Volcans d'Hawa� et ferme de caf� de Kona","/images/ile1_hawaii/touristAttraction/touristAttraction6.jpg","Historique",169.69,"Rendez-vous au parc national des volcans d'Hawa� de Big Island au d�part de Waikoloa ou de Kona lors de cette fabuleuse excursion d'une journ�e, muni de votre iPad personnel que vous pouvez utiliser durant toute votre visite�! Regardez des tortues se pr�lasser sur le sable noir de la plage de Punaluu, profitez d'un d�jeuner pique-nique et go�tez un caf� de Kona fra�chement pr�par�. Ensuite, partez en randonn�e dans un tunnel de lave et sentez la vapeur chaude du conduit du volcan, puis contemplez la lave scintillante la nuit (si la m�t�o le permet, bien s�r)�! Utilisez votre iPad pour regarder d'incroyables photos et vid�os et obtenir des informations sur les sites que vous visitez. En plus, jouez, �coutez de la musique et regardez des films � bord de votre v�hicule confortable et climatis�.",
				"19.6611759,-155.9952374",12.0);
		TouristAttraction touristAttraction7 = new TouristAttraction(29,"Hawaii Tropical Botanical Garden","/images/ile1_hawaii/touristAttraction/touristAttraction29.jpg","Loisir",0.0,"Foret tropical calme et paisant. Variétés de fleurs et d'arbres. Nature incroyable.",
				"19.6231768,-155.9242392",2);
		TouristAttraction touristAttraction8 = new TouristAttraction(30,"Hapuna Beach","/images/ile1_hawaii/touristAttraction/touristAttraction30.jpg","Loisir",0.0,"Très belle plage de sable blanc, eau très claire, peu de vagues.... Mais aucun endroit ombragé, si ce n'est le grand carbet avec ses tables pour le pique-nique... Apportez un parasol si vous comptez rester plusieurs heures! Douches et toilettes sur place.",
				"19.6231768,-155.9242392",2);
		
		TouristAttractionDAO.createTouristAttraction(touristAttraction1);
		TouristAttractionDAO.createTouristAttraction(touristAttraction2);
		TouristAttractionDAO.createTouristAttraction(touristAttraction3);
		TouristAttractionDAO.createTouristAttraction(touristAttraction4);
		TouristAttractionDAO.createTouristAttraction(touristAttraction5);
		TouristAttractionDAO.createTouristAttraction(touristAttraction6);
		TouristAttractionDAO.createTouristAttraction(touristAttraction7);
		TouristAttractionDAO.createTouristAttraction(touristAttraction8);
		
		touristAttractions.add(touristAttraction1);
		touristAttractions.add(touristAttraction2);
		touristAttractions.add(touristAttraction3);
		touristAttractions.add(touristAttraction4);
		touristAttractions.add(touristAttraction5);
		touristAttractions.add(touristAttraction6);
		touristAttractions.add(touristAttraction7);
		touristAttractions.add(touristAttraction8);
		
		return touristAttractions;
	}
	public static ArrayList<TouristAttraction> touristAttractionLoad2(){
		
		TouristAttractionDAO TouristAttractionDAO = new TouristAttractionDAOHibernate();
		ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		/*Maui Island*/
		TouristAttraction touristAttraction7 = new TouristAttraction(7,"Iao Valley State Monument ","/images/ile2_Maui/touristAttraction/touristAttraction7.gif","Historique",103.0,"D�couvrez le meilleur du centre de Maui lors de cette visite guid�e d'une journ�e panoramique et instructive, avec service de ramassage et d�p�t � l'h�tel. Votre guide local professionnel vous fera d�couvrir le crat�re volcanique de Haleakala, la vall�e de Iao, le pinacle rocheux unique, connu sous le nom d'aiguille du Lao, ainsi que la ville historique de Wailuku. Vous pourrez contempler le magnifique paysage de Maui, en apprendre davantage sur son histoire agricole et partir � la d�couverte de ses parcs uniques au monde au cours de ce tour exp�rimental. ",
				"20.8808681,-156.5468101",6.0);
		TouristAttraction touristAttraction8 = new TouristAttraction(8,"Haleakala Highway (Crater Road)","/images/ile2_Maui/touristAttraction/touristAttraction8.gif","Historique",103.0,"Le paysage lunaire du crat�re de Haleakala couvre une surface immense - si grande que Manhattan pourrait y tenir. Le plus grand volcan dormant du monde est prot�g� par le Parc National Haleakala. Cet endroit ...",
				"20.7097116,-156.2622695",8.0);
		TouristAttraction touristAttraction9 = new TouristAttraction(9,"Maui Tropical Plantation","/images/ile2_Maui/touristAttraction/touristAttraction9.gif","Historique",135.0,"Ananas juteux, fleurs d'hibiscus, poup�es hula, noix de macadamia � toutes les saveurs et beaut�s tropicales sont � l'affiche de la Plantation Tropicale de Maui. L��quipe des 24 hectares de plantation organise .",
				"20.849015,-156.5086717",1.5);
		TouristAttraction touristAttraction10 = new TouristAttraction(10,"Wai'anapanapa State Park","/images/ile2_Maui/touristAttraction/touristAttraction10.gif","Loisir",124.0,"D�couvrez la c�l�bre route de Maui qui m�ne � Hana depuis la terre et le ciel. Lors de cette visite de luxe en petit groupe, d�tendez-vous sur le trajet qui m�ne � Hana � bord d'un minibus limousine climatis� et effectuez plusieurs arr�ts en chemin : village hawa�en de Ke'anae, jardins tropicaux de Hana et magnifique cascade o� vous pourrez vous baigner. Lors de votre retour, partez pour un vol au-dessus du littoral sauvage et impr�gnez-vous des vues incomparables sur l'�le. Cette exp�rience de 6 heures sera certainement le point fort de votre s�jour � Maui !",
				"20.7836291,-156.0002193",6.0);
		TouristAttraction touristAttraction11 = new TouristAttraction(11,"Maui Ocean Center ","/images/ile2_Maui/touristAttraction/touristAttraction11.gif","Loisir",24.00,"Gr�ce � ce billet d'entr�e au Centre oc�anique de Maui, d�couvrez le plus grand aquarium de r�cif tropical de l'h�misph�re occidental ! D�couvrez un monde sous-marin magique dans ce centre impressionnant comptant plus de 60 expositions interactives. Faites participer vos sens et obtenez un aper�u de la vie marine d'Hawa� lors de votre rencontre avec des r�cifs tropicaux, des poissons, des pieuvres, des raies, des m�duses et bien plus encore.",
				"20.7926215,-156.514265",4.0);
		TouristAttraction touristAttraction12 = new TouristAttraction(12,"Skyline Eco-Adventures Zipline Tours","/images/ile2_Maui/touristAttraction/touristAttraction12.gif","Loisir",113.0,"Haleakala est le fleuron de Maui, repr�sentant pr�s de 75 % de l'�le. Au cours de cette aventure en tyrolienne, vous vous �l�verez le long des pentes de cet immense volcan ; vous volerez � travers des arbres gigantesques ; et flotterez au-dessus de bassins saisonniers sur cinq tyroliennes diff�rentes. Marchez loin au-dessus du sol de la for�t sur des ponts faits de planches de bois et impr�gnez-vous de l'incroyable paysage tropical lors de votre excursion d'une heure et demie de plaisir la t�te dans les nuages.",
				"20.8312929,-156.4707534",1.5);
	
		TouristAttractionDAO.createTouristAttraction(touristAttraction7);
		TouristAttractionDAO.createTouristAttraction(touristAttraction8);
		TouristAttractionDAO.createTouristAttraction(touristAttraction9);
		TouristAttractionDAO.createTouristAttraction(touristAttraction10);
		TouristAttractionDAO.createTouristAttraction(touristAttraction11);
		TouristAttractionDAO.createTouristAttraction(touristAttraction12);
		
		touristAttractions.add(touristAttraction7);
		touristAttractions.add(touristAttraction8);
		touristAttractions.add(touristAttraction9);
		touristAttractions.add(touristAttraction10);
		touristAttractions.add(touristAttraction11);
		touristAttractions.add(touristAttraction12);
		
		return touristAttractions;
	}
		
		/*Oahu Island*/
		public static ArrayList<TouristAttraction> touristAttractionLoad3(){
			
			TouristAttractionDAO TouristAttractionDAO = new TouristAttractionDAOHibernate();
			ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		TouristAttraction touristAttraction13 = new TouristAttraction(13,"Wet 'n' Wild Hawa�","/images/ile3_Oahu/touristAttraction/touristAttraction13.gif","Loisir",47.07,"Passez une journ�e � vous divertir avec des toboggans aquatiques et des piscines � vagues au parc aquatique Wet 'n' Wild Hawa� d'Oahu. Glissez sur des toboggans exaltants, d�tendez-vous dans la piscine � vagues, ou bien emmenez les enfants � l'aire de jeux interactive. Choisissez un pass avec entr�e g�n�rale, ajoutez le transport aller-retour � l'h�tel, ou optez pour le forfait extraordinaire qui inclut le transport, une session de surf horizontal, et un d�jeuner-buffet barbecue, boissons comprises. C'est une activit� formidable que toute la famille appr�ciera !",
				"21.3355256,-158.0888211",5.0);
		TouristAttraction touristAttraction14 = new TouristAttraction(14,"Sea Life Park dOahu","/images/ile3_Oahu/touristAttraction/touristAttraction14.gif","Loisir",66.51,"D�couvrez le plaisir d'une rencontre rapproch�e avec les requins ou les lions de mer au Sea Life Park � Hawaii. Gr�ce � la nage avec les requins, vous allez passer jusqu'� 30 minutes dans un r�servoir o� une cage m�tallique va vous s�parer de 10 requins originaires d'Hawa�. Ou, optez pour la nage avec les lions de mer pour embrasser et caresser une de ces adorables cr�atures sous-marines et apprendre les signaux manuels avec un entra�neur. Entr�e pour une journ�e compl�te au parc marin d'Oahu ; l'aquarium et le sanctuaire des oiseaux sont inclusRendez-vous au Sea Life Park d'Hawaii, situ� � 24 km de Waikiki. Assurez-vous de vous pr�senter au moins 30 minutes avant le d�but de l'activit� que vous avez r�serv�e : soit la nage avec les requins soit la nage avec les lions de mer. Vous pourrez la s�lectionner au moment de la r�servation. Chaque programme dure environ 45 minutes et inclut une s�ance d'orientation et jusqu'� 30 minutes dans l'eau. Des vestiaires sont disponibles avant et apr�s votre activit�. L'entr�e au parc est incluse ; la liste des attractions est disponible dans la section ci-dessous.",
				"21.3136925,-157.6650588",7.5);
		TouristAttraction touristAttraction15 = new TouristAttraction(15,"Visite comment�e du m�morial du cuirass� Arizona","/images/ile3_Oahu/touristAttraction/touristAttraction15.gif","Historique",6.81,"ous les visiteurs sont encourag�s � participer � la � Visite comment�e du m�morial du cuirass� USS Arizona � du service du parc national. Cette visite autoguid�e de deux heures prim�e vous offre une exp�rience enrichissante de Pearl Harbor et vous donne l'impression d'�tre accompagn� par un Ranger du parc les deux mus�es du centre des visiteurs du m�morial et le long du littoral pour le circuit � Path of Attack �. Cette visite audioguid�e est une activit� incontournable du centre des visiteurs et a joui de la participation de la c�l�bre actrice Jamie Lee Curtis, des historiens du service du parc national et de dizaines de survivants Pearl Harbor.",
				"21.3662213,-157.9486627",2.0);
		TouristAttraction touristAttraction16 = new TouristAttraction(16,"Bishop Museum","/images/ile3_Oahu/touristAttraction/touristAttraction16.gif","Historique",63.0,"Il est situ� dans le quartier historique de Kalihi d'Honolulu sur l'�le d'O'ahu. Outre les collections d'objets Hawaiiens, le Mus�um conserve la troisi�me plus importante collection d'insectes des �tats-Unis qui est riche de 13,5 millions de sp�cimens.",
				"21.3333045,-157.8731851",8.0);
		TouristAttraction touristAttraction17 = new TouristAttraction(17,"National Memorial Cemetery of the Pacific","/images/ile3_Oahu/touristAttraction/touristAttraction17.gif","Historique",43.00,"Le Cimeti�re comm�moratif national du Pacifique poss�de un espace disponible pour les restes incin�r�s dans le columbarium . Nous pouvons �tre en mesure d'accueillir les restes casketed dans la m�me tombe des membres de la famille d�j� enterr�s . P�riodiquement , cependant , l'espace d'enfouissement peuvent devenir disponibles en raison de l'exhumation d'une tombe existante ou pour d'autres raisons . Si l'espace d'enfouissement est disponible au moment de la demande , le cimeti�re attribuera une tombe � un ancien combattant admissible ou membre de la famille . Comme il n'y a aucun moyen de savoir � l'avance quand une tombe peut devenir disponible , s'il vous pla�t contacter le cimeti�re au moment de la n�cessit� de se demander si l'espace est disponible .",
				"21.3125999,-157.8481078",8.0);
		TouristAttraction touristAttraction18 = new TouristAttraction(18,"Aloha Food Tours","/images/ile3_Oahu/touristAttraction/touristAttraction18.gif","Restaurant",100.0,"La nourriture d�licieuse (et il y a plein de l'h�tel), super guide touristique qui sait la ville et nous a donn� beaucoup d'informations ",
				"21.3226168,-157.8535166",5.0);
		
		TouristAttractionDAO.createTouristAttraction(touristAttraction13);
		TouristAttractionDAO.createTouristAttraction(touristAttraction14);
		TouristAttractionDAO.createTouristAttraction(touristAttraction15);
		TouristAttractionDAO.createTouristAttraction(touristAttraction16);
		TouristAttractionDAO.createTouristAttraction(touristAttraction17);
		TouristAttractionDAO.createTouristAttraction(touristAttraction18);
		
		touristAttractions.add(touristAttraction13);
		touristAttractions.add(touristAttraction14);
		touristAttractions.add(touristAttraction15);
		touristAttractions.add(touristAttraction16);
		touristAttractions.add(touristAttraction17);
		touristAttractions.add(touristAttraction18);
		
		return touristAttractions;
		}
		
		/*Kauai Island*/
		public static ArrayList<TouristAttraction> touristAttractionLoad4(){
			
			TouristAttractionDAO TouristAttractionDAO = new TouristAttractionDAOHibernate();
			ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		TouristAttraction touristAttraction19 = new TouristAttraction(19,"Kilohana Plantation Estate","/images/ile4_Kauai/touristAttraction/touristAttraction19.gif","Historique",103.0,"il n'a lieu que dans les �les Hawa� o� vous pouvez monter un train , randonn�e � travers une for�t tropicale, go�ter les spiritueux fabriqu�s localement , boutique, d�ner ou profiter d'un spa de jour ... Vous allez adorer la chance de voir un morceau de l'histoire de Kauai tout en d�couvrant le meilleur de l'�le aujourd'hui!",
				"21.9721741,-159.3927458",4.0);
		TouristAttraction touristAttraction20 = new TouristAttraction(20,"Waimea Canyon","/images/ile4_Kauai/touristAttraction/touristAttraction20.gif","Loisir",95.17,"Profitez d'une journ�e riche de beaut�, l�gendes et histoires de Kauai, surnomm�e � l'�le jardin �, lors de cette excursion au canyon de Waimea ! Cette visite panoramique guid�e vous emm�ne au canyon de Waimea et au Wailua, avec de nombreux arr�ts photo le long du trajet.",
				"22.0567052,-159.3486237",9.5);
		TouristAttraction touristAttraction21 = new TouristAttraction(21,"Na Pali Coast State Park","/images/ile4_Kauai/touristAttraction/touristAttraction21.gif","Loisir",109.0,"La c�te de Kauai Na Pali est c�l�bre pour la beaut� de son front marin, la vie marine et les sports d'eau. La c�te de 24 Kms est bord�e de falaises, plages de sable blanc et mer turquoise.",
				"22.1737297,-159.6415189",2.0);
		TouristAttraction touristAttraction22 = new TouristAttraction(22,"Kilauea Point National Wildlife Refuge","/images/ile4_Kauai/touristAttraction/touristAttraction22.gif","Historique",212.0,"Voir le meilleur de Kauai sur cette visite priv�e ! Choisissez de voir les c�tes Est et du Nord ou les rives ouest et sud et d'explorer des villes comme Princeville , Kapaa , Waimea et Wailua . Vous aurez la possibilit� de personnaliser votre itin�raire avec l'aide de votre guide convivial , qui vous prendra des plages pittoresques , des cascades et des r�serves fauniques .",
				"22.23175,-159.4042827",8.0);
		TouristAttraction touristAttraction23 = new TouristAttraction(23,"Ke'anae Arboretum","/images/ile4_Kauai/touristAttraction/touristAttraction23.gif","Loisir",280.0,"Ke'anae Arboretum ( 6 acres ou 2,4 hectares ) est un arboretum et un jardin botanique situ� sur la route de Hana (route 360 ) d'environ 1 mile (1,6 km) � l'ouest de Ke'anae , Maui , Hawaii , �tats-Unis . Il est ouvert tous les jours sans frais.L'arboretum se trouve � c�t� du Piinaau Stream taro terrasses de culture et dans une for�t tropicale , et contient deux courts sentiers de randonn�e . Il contient environ 150 vari�t�s de plantes tropicales , y compris gingembres , hibiscus, papaye, et divers types de taro , ainsi que des arbres indig�nes et introduites dans un cadre naturel .",
				"20.8447764,-156.1452022",8.0);
		TouristAttraction touristAttraction24 = new TouristAttraction(24,"Kalalau Lookout","/images/ile4_Kauai/touristAttraction/touristAttraction24.gif","Historique",100, "La vall�e de Kalalau est situ� sur le c�t� nord-ouest de l'�le de Kaua'i dans l'�tat d'Hawaii . La vall�e est situ�e dans le State Park Na Pali Coast et abrite la magnifique plage de Kalalau . La c�te de Na Pali est tr�s robuste et est inaccessible aux voitures . Les seuls moyens l�gaux pour acc�der � la vall�e sont en kayak ou en randonn�e du sentier Kalalau .La vall�e est connue pour sa beaut� naturelle ; il est entour� de falaises verdoyantes de plus de 2000 pieds ( 610 m) de haut . Le fond de cette vall�e est large et relativement plate , avec une r�gion accessible environ 2 miles ( 3,2 km) de long et de 0,5 miles ( 0,80 km) de large. Le soleil abondant et la pluie fournit un environnement id�al pour la flore et la faune . De nombreux Hawa�ens autochtones vivaient dans la vall�e dans le 20�me si�cle , l'agriculture taro � partir d'un vaste complexe de champs en terrasses . Aujourd'hui , sa d�signation comme un parc d'�tat interdit � quiconque de r�sider l�.",
				"22.1511347,-159.648148",7.0);
		
		TouristAttractionDAO.createTouristAttraction(touristAttraction19);
		TouristAttractionDAO.createTouristAttraction(touristAttraction20);
		TouristAttractionDAO.createTouristAttraction(touristAttraction21);
		TouristAttractionDAO.createTouristAttraction(touristAttraction22);
		TouristAttractionDAO.createTouristAttraction(touristAttraction23);
		TouristAttractionDAO.createTouristAttraction(touristAttraction24);
		
		touristAttractions.add(touristAttraction19);
		touristAttractions.add(touristAttraction20);
		touristAttractions.add(touristAttraction21);
		touristAttractions.add(touristAttraction22);
		touristAttractions.add(touristAttraction23);
		touristAttractions.add(touristAttraction24);
		
		return touristAttractions;
		}
		
		/*Molokai Island*/
		public static ArrayList<TouristAttraction> touristAttractionLoad5(){
			
			TouristAttractionDAO TouristAttractionDAO = new TouristAttractionDAOHibernate();
			ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		TouristAttraction touristAttraction25 = new TouristAttraction(25,"Halawa Valley","/images/ile5_Molokai/touristAttraction/touristAttraction25.gif","Loisir",280.0,"Halawa est une vall�e et ahupua'a ( division traditionnelle des terres ) � l'extr�mit� orientale de l'�le de Molokai � Hawaii , �tats-Unis .La vall�e se prolonge environ 2 miles � l'int�rieur de la mer . A la t�te de la vall�e sont deux chutes d'eau , le Mo'aula Chutes , 250 pieds ( 76 m) de haut , et la Hipuapua Falls, 500 pieds ( 150 m) de haut .",
				"21.1358094,-157.1502343",7.0);
		TouristAttraction touristAttraction26 = new TouristAttraction(26,"Kalaupapa National Historical Park","/images/ile5_Molokai/touristAttraction/touristAttraction26.gif","Loisir",59.00,"Pendant votre s�jour � Molokai, vous devez absolument visiter Kalaupapa, c�l�bre campement de l�preux dans la p�ninsule de Makanalua, sur le littoral nord de Molokai. Apr�s une incroyable �pid�mie de ce qu'on appelle aussi la maladie de Hansen, les l�preux durent se se retirer vers ce site sur l'�le de Molokai. Entour�e par la mer sur trois c�t�s et par d'insurmontables falaises sur le quatri�me, Kalaupapa est on ne peut plus � l'�cart. P�re Damien, pr�tre belge, et bien d'autres s'occup�rent de la communaut� en consacrant leur vie au d�pit des risques de contracter la l�pre. Aujourd'hui, Kalaupapa b�n�ficie du statut de Site Historique National et abrite d'anciens patients ayant eu la maladie de Hansen. Nous vous conseillons de vous y rendre en mulet ou � pied et de ressentir les souvenirs solennels de soins et gu�rison apport�s aux malades dont le lieu est impr�gn�.",
				"21.1902791,-156.986042",8.0);
		TouristAttraction touristAttraction27 = new TouristAttraction(27,"St. Joseph's Catholic Church ","/images/ile5_Molokai/touristAttraction/touristAttraction27.gif","Historique",0.0,"Un endroit merveilleux pour d�couvrir une partie de l'histoire de Molokai.",
				"21.091182,-157.0221658",1.5);
		TouristAttraction touristAttraction28 = new TouristAttraction(28,"St. Philomena Church","/images/ile5_Molokai/touristAttraction/touristAttraction28.gif","Historique",0.0,"D�couvrir une partie de l'histoire de Molokaiet la tombe du p�re Damien.",
				"21.1352841,-157.1502337",1.5);
		
		TouristAttractionDAO.createTouristAttraction(touristAttraction25);
		TouristAttractionDAO.createTouristAttraction(touristAttraction26);
		TouristAttractionDAO.createTouristAttraction(touristAttraction27);
		TouristAttractionDAO.createTouristAttraction(touristAttraction28);
		
		touristAttractions.add(touristAttraction25);
		touristAttractions.add(touristAttraction26);
		touristAttractions.add(touristAttraction27);
		touristAttractions.add(touristAttraction28);
		
		return touristAttractions;
	}
}
