package test.database;

import java.io.IOException;
import java.util.ArrayList;

import persistence.data.Hotel;
import persistence.data.Hub;
import persistence.data.Island;
import persistence.data.Line;
import persistence.data.Restaurant;
import persistence.data.TouristAttraction;
import persistence.hibernate.HiberanteDBInit;


public class LoadData {

	public static void main(String[] args) throws IOException {
		HiberanteDBInit.createTables();
		Line airline = LineLoadData.loadAirLine();
		Line sealine = LineLoadData.loadSeaLine();
		
		/* ile Hawai */ 
		ArrayList<TouristAttraction> touristAttractionIsland1 = TouristAttractionLoadData.touristAttractionLoad1();
		ArrayList<Restaurant> restaurantIsland1 =RestaurantLoadData.restaurantLoad1();
		ArrayList<Hotel> hotelIsland1 = HotelLoadData.loadHotel1();
		Island island1 = IslandLoadData.loadIsland1(touristAttractionIsland1, hotelIsland1, restaurantIsland1);
		ArrayList<Hub> hubIsland1 = HubLoadData.loadAirport1(airline,island1);
		hubIsland1.addAll(HubLoadData.loadPort1(sealine,island1));
			
		/* ile Maui */
		ArrayList<TouristAttraction> touristAttractionIsland2 = TouristAttractionLoadData.touristAttractionLoad2();
		ArrayList<Restaurant> restaurantIsland2 =RestaurantLoadData.restaurantLoad2();
		ArrayList<Hotel> hotelIsland2 =HotelLoadData.loadHotel2();
		Island island2 = IslandLoadData.loadIsland2(touristAttractionIsland2, hotelIsland2, restaurantIsland2);
		ArrayList<Hub> hubIsland2 = HubLoadData.loadAirport2(airline,island2);
		hubIsland2.addAll(HubLoadData.loadPort2(sealine,island2));

		/* ile Oahu */ 
		
		ArrayList<TouristAttraction> touristAttractionIsland3 = TouristAttractionLoadData.touristAttractionLoad3();
		ArrayList<Restaurant> restaurantIsland3 =RestaurantLoadData.restaurantLoad3();
		ArrayList<Hotel> hotelIsland3 =HotelLoadData.loadHotel3();
		Island island3 = IslandLoadData.loadIsland3(touristAttractionIsland3, hotelIsland3, restaurantIsland3);
		ArrayList<Hub> hubIsland3 = HubLoadData.loadAirport3(airline,island3);
		hubIsland3.addAll(HubLoadData.loadPort3(sealine,island3));
		
		/* ile Kauai */
		
		ArrayList<TouristAttraction> touristAttractionIsland4 = TouristAttractionLoadData.touristAttractionLoad4();
		ArrayList<Restaurant> restaurantIsland4 =RestaurantLoadData.restaurantLoad4();
		ArrayList<Hotel> hotelIsland4 =HotelLoadData.loadHotel4();
		Island island4 = IslandLoadData.loadIsland4(touristAttractionIsland4, hotelIsland4, restaurantIsland4);
		ArrayList<Hub> hubIsland4 = HubLoadData.loadAirport4(airline,island4);
		hubIsland4.addAll(HubLoadData.loadPort4(sealine,island4));
		
		/* ile Molokai */ 
		
		ArrayList<TouristAttraction> touristAttractionIsland5 = TouristAttractionLoadData.touristAttractionLoad5();
		ArrayList<Restaurant> restaurantIsland5 =RestaurantLoadData.restaurantLoad5();
		ArrayList<Hotel> hotelIsland5 =HotelLoadData.loadHotel5();
		Island island5 = IslandLoadData.loadIsland5(touristAttractionIsland5, hotelIsland5, restaurantIsland5);
		ArrayList<Hub> hubIsland5 = HubLoadData.loadAirport5(airline,island5);
		hubIsland5.addAll(HubLoadData.loadPort5(sealine,island5));
		
	}
}
