package test.database;


import java.util.ArrayList;

import persistence.data.Hotel;
import persistence.hibernate.HotelDAOHibernate;


public class HotelLoadData {
	
	/*ile hawaii*/
	public static ArrayList<Hotel> loadHotel1(){
	
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		HotelDAOHibernate hotelDAO = new HotelDAOHibernate();
		
		Hotel hotel1 = new Hotel(1, "Four Seasons Resort Hualalai", "Kailua-Kona, HI 96740�tats-Unis", 860.00, 5, "19.8282367,-155.9925328","/images/ile1_hawaii/hotel/hotel1.gif");
		Hotel hotel2 = new Hotel(2, "Mauna Lani Bay Hotel & Bungalows", "68-1400 Mauna Lani Dr, Waimea, HI 96743, �tats-Unis", 295.0, 4, "19.946247,-155.8693607","/images/ile1_hawaii/hotel/hotel2.gif");
		Hotel hotel3 = new Hotel(3, "Holiday Inn Express Hotel & Suites Kailua-Kona", "75-146 Sarona Rd, Kailua-Kona, HI 96740, �tats-Unis",149.00, 4, "19.6388428,-155.9937814","/images/ile1_hawaii/hotel/hotel3.gif");
		Hotel hotel4 = new Hotel(4, "Waikoloa Beach Marriott Resort & Spa", "9-275 Waikoloa Beach Dr, Waikoloa Village, HI 96738, �tats-Unis", 249.00, 4, "19.916248,-155.8869267","/images/ile1_hawaii/hotel/hotel4.gif");
		Hotel hotel5 = new Hotel(5, "Chalet Kilauea", "19-4178 Wright Rd, Volcano, HI 96785, �tats-Unis", 72.00, 3, "19.442251,-155.2376877","/images/ile1_hawaii/hotel/hotel5.gif");
		
		hotelDAO.createHotel(hotel1);
		hotelDAO.createHotel(hotel2);
		hotelDAO.createHotel(hotel3);
		hotelDAO.createHotel(hotel4);
		hotelDAO.createHotel(hotel5);
		
		hotels.add(hotel1);
		hotels.add(hotel2);
		hotels.add(hotel3);
		hotels.add(hotel4);
		hotels.add(hotel5);
		
		return hotels;

	}
	
	/*ile Maui*/
	public static ArrayList<Hotel> loadHotel2(){
		
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		HotelDAOHibernate hotelDAO = new HotelDAOHibernate();
		
		Hotel hotel6 = new Hotel(6, "Napili Kai Beach Resort", "19-4178 Wright Rd, Volcano, HI 96785, �tats-Unis", 76.10, 3, "19.4417609,-155.2371411","/images/ile2_Maui/hotel/hotel6.gif");
		Hotel hotel7 = new Hotel(7, "Four Seasons Resort Maui at Wailea", "3900 Wailea Alanui Dr, Wailea, HI 96753, �tats-Unis", 504.0, 5, "20.6805021,-156.444433","/images/ile2_Maui/hotel/hotel7.gif");
		Hotel hotel8 = new Hotel(8, "Marriott's Maui Ocean Club - Lahaina & Napili Towers", "100 Nohea Kai Dr, Lahaina, HI 96761, �tats-Unis",474.89, 5, "20.9164704,-156.6972339","/images/ile2_Maui/hotel/hotel8.gif");
		Hotel hotel9 = new Hotel(9, "BEST WESTERN Pioneer Inn", "658 Wharf St, Lahaina, HI 96761, �tats-Unis", 182.00, 4, "20.872233,-156.6804477","/images/ile2_Maui/hotel/hotel9.gif");
		Hotel hotel10 = new Hotel(10, "Napili Shores", "5315 Lower Honoapiilani Rd, Lahaina, HI 96761, �tats-Unis", 196.00, 4, "20.9925192,-156.6692494","/images/ile2_Maui/hotel/hotel10.gif");
		
		hotelDAO.createHotel(hotel6);
		hotelDAO.createHotel(hotel7);
		hotelDAO.createHotel(hotel8);
		hotelDAO.createHotel(hotel9);
		hotelDAO.createHotel(hotel10);
		
		hotels.add(hotel6);
		hotels.add(hotel7);
		hotels.add(hotel8);
		hotels.add(hotel9);
		hotels.add(hotel10);
		
		return hotels;


	}
	
	/* ile Oahu */
	
	public static ArrayList<Hotel> loadHotel3(){
		
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		HotelDAOHibernate hotelDAO = new HotelDAOHibernate();
		
		Hotel hotel11 = new Hotel(11, "The Modern Honolulu", "1775 Ala Moana Blvd, Honolulu, HI 96815, �tats-Unis", 283.0,4, "21.284982,-157.8413407","/images/ile3_Oahu/hotel/hotel11.gif");
		Hotel hotel12 = new Hotel(12, "Halekulani Hotel", "2199 Kalia Rd, Honolulu, HI 96815, �tats-Unis", 518.00, 5, "21.27797,-157.8341721","/images/ile3_Oahu/hotel/hotel12.gif");
		Hotel hotel13 = new Hotel(13, "Lotus Honolulu at Diamond Head", "2885 Kalakaua Ave, Honolulu, HI 96815, �tats-Unis",246.50, 4, "21.262955,-157.8229406","/images/ile3_Oahu/hotel/hotel13.gif");
		Hotel hotel14 = new Hotel(14, "Embassy Suites by Hilton Waikiki Beach Walk", "201 Beach Walk, Honolulu, HI 96815, �tats-Unis",456.0, 4, "21.278989,-157.8338787","/images/ile3_Oahu/hotel/hotel14.gif");
		Hotel hotel15 = new Hotel(15, "Hokulani Waikiki by Hilton Grand Vacations","2181 Kalakaua Ave, Honolulu, HI 96815, �tats-Unis", 363.00, 4, "21.280018,-157.8323907","/images/ile3_Oahu/hotel/hotel15.gif");
		
		hotelDAO.createHotel(hotel11);
		hotelDAO.createHotel(hotel12);
		hotelDAO.createHotel(hotel13);
		hotelDAO.createHotel(hotel14);
		hotelDAO.createHotel(hotel15);
		
		hotels.add(hotel11);
		hotels.add(hotel12);
		hotels.add(hotel13);
		hotels.add(hotel14);
		hotels.add(hotel15);
		
		return hotels;

	}
	
	/* ile Kauai */
	public static ArrayList<Hotel> loadHotel4(){
		
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		HotelDAOHibernate hotelDAO = new HotelDAOHibernate();
		
		Hotel hotel16 = new Hotel(16, "Koa Kea Hotel & Resort", "2251 Poipu Road, Poipu, Kauai, HI 96756, �tats-Unis", 409.0,4, "21.8751039,-159.4600271","/images/ile4_Kauai/hotel/hotel16.gif");
		Hotel hotel17 = new Hotel(17, "Grand Hyatt Kauai Resort and Spa", "1571 Poipu Rd, Poipu, Kauai, HI 96756, �tats-Unis", 448.0, 4, "21.875309,-159.4419667","/images/ile4_Kauai/hotel/hotel17.gif");
		Hotel hotel18 = new Hotel(18, "Hanalei Bay Resort", "5380 Honoiki Rd, Princeville, HI 96722, �tats-Unis",218.0, 4, "22.2195192,-159.4959014","/images/ile4_Kauai/hotel/hotel18.gif");
		Hotel hotel19 = new Hotel(19, "Castle at Princeville", "5300 Ka Haku Road, Suite C, Princeville, HI 96722, �tats-Unis",312.0, 4, "22.225096,-159.4929887","/images/ile4_Kauai/hotel/hotel19.gif");
		Hotel hotel20 = new Hotel(20, "Aston Islander on the Beach","440 Aleka Pl, Kapaa, HI 96746, �tats-Unis", 137.00, 4, "22.053021,-159.3304699","/images/ile4_Kauai/hotel/hotel20.gif");
		
		hotelDAO.createHotel(hotel16);
		hotelDAO.createHotel(hotel17);
		hotelDAO.createHotel(hotel18);
		hotelDAO.createHotel(hotel19);
		hotelDAO.createHotel(hotel20);
		
		hotels.add(hotel16);
		hotels.add(hotel17);
		hotels.add(hotel18);
		hotels.add(hotel19);
		hotels.add(hotel20);
		
		return hotels;
	}
	
	public static ArrayList<Hotel> loadHotel5(){
		
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		HotelDAOHibernate hotelDAO = new HotelDAOHibernate();
		
		Hotel hotel21 = new Hotel(21, "Hotel Molokai", "1300 Kamehameha V Hwy, Kaunakakai, HI 96748, �tats-Unis", 142.0,3, "21.078731,-156.998188","/images/ile5_Molokai/hotel/hotel21.gif");
		Hotel hotel22 = new Hotel(22, "Molokai Hilltop Cottage & West End Studio ", "675 W Papa Ave, Kahului, HI 96732, �tats-Unis", 112.0, 4, "20.872886,-156.4804347","/images/ile5_Molokai/hotel/hotel22.gif");

		hotelDAO.createHotel(hotel21);
		hotelDAO.createHotel(hotel22);

		hotels.add(hotel21);
		hotels.add(hotel22);

		
		return hotels;

	}


}
