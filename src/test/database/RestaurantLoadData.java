package test.database;

import java.util.ArrayList;

import dao.RestaurantDAO;

import persistence.data.Restaurant;
import persistence.hibernate.RestaurantDAOHibernate;


public class RestaurantLoadData {
	
	public static ArrayList<Restaurant> restaurantLoad1(){
		RestaurantDAO restaurantDAO = new RestaurantDAOHibernate();
		ArrayList<Restaurant> restaurants=new ArrayList<Restaurant>();
		/*Hawai Island*/
		
		Restaurant restaurant1 = new Restaurant(1,"TJ'S BBQ by the beach","75-6129 Alii Dr, Kailua-Kona, HI 96740, �tats-Unis",20.5,"Americaine",5,"/images/ile1_hawaii/restaurant/restaurant1.gif","19.6138857,-155.9830766");
		Restaurant restaurant2 = new Restaurant(2,"Paul's Place","132 Punahoa St, Hilo, HI 96720, �tats-Unis",15.5,"Americaine",4,"/images/ile1_hawaii/restaurant/restaurant2.gif","19.722587,-155.0865067");
		Restaurant restaurant3 = new Restaurant(3,"Sushi Rock and Trio ","55-3435 Akoni Pule Hwy, Hawi, HI 96719, �tats-Unis",18.0,"Japonaise",4,"/images/ile1_hawaii/restaurant/restaurant3.gif","20.2382186,-155.8317651");
		Restaurant restaurant4 = new Restaurant(4,"Umekes Ali'i Plaza","75-143 Hualalai Rd #105, Kailua-Kona, HI 96740, �tats-Unis",22.5,"Hawaiienne",4,"/images/ile1_hawaii/restaurant/restaurant4.gif","19.6411132,-155.9944068");
		Restaurant restaurant5 = new Restaurant(5,"Ohelo Cafe","19-4005 Haunani Rd, Volcano, HI 96785, �tats-Unis",17.5,"Italien",3,"/images/ile1_hawaii/restaurant/restaurant5.gif","19.4283977,-155.2376928");
		
		restaurantDAO.createRestaurant(restaurant1);
		restaurantDAO.createRestaurant(restaurant2);
		restaurantDAO.createRestaurant(restaurant3);
		restaurantDAO.createRestaurant(restaurant4);
		restaurantDAO.createRestaurant(restaurant5);
		
		restaurants.add(restaurant1);
		restaurants.add(restaurant2);
		restaurants.add(restaurant3);
		restaurants.add(restaurant4);
		restaurants.add(restaurant5);
		
		return restaurants;
	}
		/*Maui Island*/
	public static ArrayList<Restaurant> restaurantLoad2(){
		
		RestaurantDAO restaurantDAO = new RestaurantDAOHibernate();
		ArrayList<Restaurant> restaurants=new ArrayList<Restaurant>();
		
		Restaurant restaurant6 = new Restaurant(6,"Mana Foods","49 Baldwin Ave, Paia, HI 96779, �tats-Unis",15.5,"Vegetarien",3,"/images/ile2_Maui/restaurant/restaurant6.gif","20.9154945,-156.382232");
		Restaurant restaurant7 = new Restaurant(7,"Mama's Fish House","799 Poho Pl, Paia, HI 96779, �tats-Unis",45.5,"Hawaienne",4,"/images/ile2_Maui/restaurant/restaurant7.gif","20.929024,-156.3691927");
		Restaurant restaurant8 = new Restaurant(8,"Cuatro Restaurant","Kihei Town Center Shopping Center, 1881 S Kihei Rd #111, Kihei, HI 96753, �tats-Unis",22.5,"Americaine",3,"/images/ile2_Maui/restaurant/restaurant8.gif","20.7336635,-156.4543814");
		Restaurant restaurant9 = new Restaurant(9,"Coconut's Fish Cafe","1279 S Kihei Rd, Kihei, HI 96753, �tats-Unis",10.5,"Americaine",3,"/images/ile2_Maui/restaurant/restaurant9.gif","20.750134,-156.4573127");
		Restaurant restaurant10 = new Restaurant(10,"Kula Bistro","4566 Lower Kula Rd, Kula, HI 96790, �tats-Unis",12.0,"Americaine",3,"/images/ile2_Maui/restaurant/restaurant10.gif","20.7596913,-156.3297338");
		
		restaurantDAO.createRestaurant(restaurant6);
		restaurantDAO.createRestaurant(restaurant7);
		restaurantDAO.createRestaurant(restaurant8);
		restaurantDAO.createRestaurant(restaurant9);
		restaurantDAO.createRestaurant(restaurant10);
		
		restaurants.add(restaurant6);
		restaurants.add(restaurant7);
		restaurants.add(restaurant8);
		restaurants.add(restaurant9);
		restaurants.add(restaurant10);
		
		return restaurants;
	}	
		/*Oahu Island*/
	public static ArrayList<Restaurant> restaurantLoad3(){
		RestaurantDAO restaurantDAO = new RestaurantDAOHibernate();
		ArrayList<Restaurant> restaurants=new ArrayList<Restaurant>();
		
		Restaurant restaurant11 = new Restaurant(11,"Ono Seafood","747 Kapahulu Ave, Honolulu, HI 96816, �tats-Unis",13.50,"Hawaienne",3,"/images/ile3_Oahu/restaurant/restaurant11.gif","21.2810964,-157.8159843");
		Restaurant restaurant12 = new Restaurant(12,"The Pig and the Lady","83 N King St, Honolulu, HI 96817, �tats-Unis",28.5,"Asiatique Vietnamien",4,"/images/ile3_Oahu/restaurant/restaurant12.gif","21.3114482,-157.8658372");
		Restaurant restaurant13 = new Restaurant(13,"Kalapawai Cafe & Deli","750 Kailua Rd, Kailua, HI 96734, �tats-Unis",18.75,"Americaine",4,"/images/ile3_Oahu/restaurant/restaurant13.gif","21.3935659,-157.7471614");
		Restaurant restaurant14 = new Restaurant(14,"Alan Wong's Restaurant ","1857 S King St #208, Honolulu, HI 96826, �tats-Unis",29.00,"Americaine",3,"/images/ile3_Oahu/restaurant/restaurant14.gif","21.2951601,-157.8338786");
		Restaurant restaurant15 = new Restaurant(15,"Lei Lei's Bar & Grill","57-049 Kuilima Dr, Kahuku, HI 96731, �tats-Unis",15.50,"Americaine",3,"/images/ile3_Oahu/restaurant/restaurant15.gif","21.701863,-157.9980767");
		
		restaurantDAO.createRestaurant(restaurant11);
		restaurantDAO.createRestaurant(restaurant12);
		restaurantDAO.createRestaurant(restaurant13);
		restaurantDAO.createRestaurant(restaurant14);
		restaurantDAO.createRestaurant(restaurant15);
		
		restaurants.add(restaurant11);
		restaurants.add(restaurant12);
		restaurants.add(restaurant13);
		restaurants.add(restaurant14);
		restaurants.add(restaurant15);
		
		return restaurants;

	}
		/*Kauai Island*/
	public static ArrayList<Restaurant> restaurantLoad4(){
		
		RestaurantDAO restaurantDAO = new RestaurantDAOHibernate();
		ArrayList<Restaurant> restaurants=new ArrayList<Restaurant>();
		
		Restaurant restaurant16 = new Restaurant(16,"JO2 Natural Cuisine","4-971 Kuhio Hwy, Kapaa, HI 96746, �tats-Unis",17.50,"V�g�tarienne",4,"/images/ile4_Kauai/restaurant/restaurant16.gif","22.0655445,-159.321799");
		Restaurant restaurant17 = new Restaurant(17,"Makai Sushi ","2827 Poipu Rd, Koloa, HI 96756, �tats-Unis",23.5,"Americaine",4,"/images/ile4_Kauai/restaurant/restaurant17.gif","21.8848779,-159.4704887");
		Restaurant restaurant18 = new Restaurant(18,"Tacos Al Pastor","Kuhio Hwy, Kapaa, HI 96746, �tats-Unis",15.0,"Mexicaine",4,"/images/ile4_Kauai/restaurant/restaurant18.gif","22.0808407,-159.3157487");
		Restaurant restaurant19 = new Restaurant(19,"La Spezia Restaurant and Wine Bar","5492 Koloa Rd, Koloa, HI 96756, �tats-Unis",17.5,"Italienne",4,"/images/ile4_Kauai/restaurant/restaurant19.gif","21.9049094,-159.4656999");
		Restaurant restaurant20 = new Restaurant(20,"The Fish Express","3343 Kuhio Hwy, Lihue, HI 96766, �tats-Unis",7.50,"Hawaienne",5,"/images/ile4_Kauai/restaurant/restaurant20.gif","21.9841318,-159.3692852");
		
		restaurantDAO.createRestaurant(restaurant16);
		restaurantDAO.createRestaurant(restaurant17);
		restaurantDAO.createRestaurant(restaurant18);
		restaurantDAO.createRestaurant(restaurant19);
		restaurantDAO.createRestaurant(restaurant20);
		
		restaurants.add(restaurant16);
		restaurants.add(restaurant17);
		restaurants.add(restaurant18);
		restaurants.add(restaurant19);
		restaurants.add(restaurant20);
		
		return restaurants;

	}
		/*Molokai Island*/
	public static ArrayList<Restaurant> restaurantLoad5(){
		
		RestaurantDAO restaurantDAO = new RestaurantDAOHibernate();
		ArrayList<Restaurant> restaurants=new ArrayList<Restaurant>();
		Restaurant restaurant21 = new Restaurant(21,"Molokai Pizza Cafe","15 Kaunakakai Pl, Kaunakakai, HI 96748, �tats-Unis",15.50,"Italienne",3,"/images/ile5_Molokai/restaurant/restaurant21.gif","21.0887901,-157.0243927");
		Restaurant restaurant22 = new Restaurant(22,"Molokai Burger","20 Kamehameha V Hwy, Kaunakakai, HI 96748, �tats-Unis",12.50,"Americaine",3,"/images/ile5_Molokai/restaurant/restaurant22.gif","21.089189,-157.0244387");
		Restaurant restaurant23 = new Restaurant(23,"Maka's Korner","35 Mohala St, Kaunakakai, HI 96748, �tats-Unis",12.50,"Hawaiienne",4,"/images/ile5_Molokai/restaurant/restaurant23.gif","21.0900054,-157.0228005");
		Restaurant restaurant24 = new Restaurant(24,"Hale Kealoha Restaurant & Bar at Hotel Molokai ","120 Hekili St, Kailua, HI 96734, �tats-Unis",11.50,"Americaine",3,"/images/ile5_Molokai/restaurant/restaurant24.gif","21.3924492,-157.7441206");
		Restaurant restaurant25 = new Restaurant(25,"Hula Shores ","1300 Kamehameha V Hwy, Kaunakakai, HI 96748, �tats-Unis",18.0,"Americaine",5,"/images/ile5_Molokai/restaurant/restaurant25.gif","21.0788829,-156.9982707");
		
		restaurantDAO.createRestaurant(restaurant21);
		restaurantDAO.createRestaurant(restaurant22);
		restaurantDAO.createRestaurant(restaurant23);
		restaurantDAO.createRestaurant(restaurant24);
		restaurantDAO.createRestaurant(restaurant25);
		
		restaurants.add(restaurant21);
		restaurants.add(restaurant22);
		restaurants.add(restaurant23);
		restaurants.add(restaurant24);
		restaurants.add(restaurant25);
		
		return restaurants;

	}

}
