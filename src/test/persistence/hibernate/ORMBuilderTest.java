package test.persistence.hibernate;

import java.util.ArrayList;

import buisness.model.Hotel;
import buisness.model.Line;
import buisness.model.Restaurant;
import buisness.model.TouristAttraction;
import junit.framework.TestCase;
import persistence.hibernate.ORMBuilder;

public class ORMBuilderTest extends TestCase {
	
	public static void main(String[] args){
		junit.textui.TestRunner.run(ORMBuilderTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
	}
	
	protected void tearDown() throws Exception {
        super.tearDown();
	}
	
	public void testbuildTouristAttractionData(){
		ArrayList<TouristAttraction> touristAttractions = ORMBuilder.getInstance().buildTouristAttractionData();
		assertEquals(28, touristAttractions.size());
		assertEquals("Hilo Hot Steam Volcano Tour", touristAttractions.get(3).getNameTouristAttraction());
	}
	
	public void testbuildRestaurantData(){
		ArrayList<Restaurant> restaurants = ORMBuilder.getInstance().buildResturantData();
		assertEquals(25, restaurants.size());
		assertEquals( "TJ'S BBQ by the beach", restaurants.get(0).getNameRestaurant());
	}
	
	public void testbuildHotelData(){
		ArrayList<Hotel> hotels = ORMBuilder.getInstance().buildHotelData();
		assertEquals(22, hotels.size());
		assertEquals( "Four Seasons Resort Hualalai", hotels.get(0).getNameHotel());
	}
	
	public void testbuildLine(){
		ArrayList<Line> lines = ORMBuilder.getInstance().buildLineData();
		assertEquals("Airplane", lines.get(0).getTypeTransport());
	}
	

}
