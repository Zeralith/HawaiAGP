package test.dao;

import java.util.List;

import junit.framework.TestCase;
import persistence.data.TouristAttraction;
import persistence.hibernate.TouristAttractionDAOHibernate;

public class TouristAttractionDAOTest extends TestCase {
	
	public static void main(String args[]){
		junit.textui.TestRunner.run(TouristAttractionDAOTest.class);
	}
	protected void setUp() throws Exception {
        super.setUp();
        //Data must Be loaded
	}
	
	protected void tearDown() throws Exception {
        super.tearDown();
	}
	
	public void testTouristAttractionAllGet(){
		TouristAttractionDAOHibernate touristAttractionDAO = new TouristAttractionDAOHibernate();
		List<TouristAttraction> touristAttractions = touristAttractionDAO.getAllTouristAttraction();
		assertEquals(28, touristAttractions.size());
	}
	
	public void testTouristAttractionGet(){
		TouristAttractionDAOHibernate touristAttractionDAO = new TouristAttractionDAOHibernate();
		TouristAttraction touristAttraction = touristAttractionDAO.searchTouristAttractionbyName("Aventure aux volcans hawa�ens de Big Island");
		assertEquals(1, touristAttraction.getIdAttraction());
	}
}
