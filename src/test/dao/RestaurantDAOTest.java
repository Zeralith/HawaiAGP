package test.dao;

import java.util.ArrayList;

import junit.framework.Assert;
import junit.framework.TestCase;
import persistence.data.Restaurant;
import persistence.hibernate.RestaurantDAOHibernate;

public class RestaurantDAOTest extends TestCase {
	
	public static void main(String[] args){
		junit.swingui.TestRunner.run(RestaurantDAOTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
        //Data must Be loaded
	}
	
	protected void tearDown() throws Exception {
        super.tearDown();
	}
	
	public void testSearchRestaurantByName(){
		RestaurantDAOHibernate restaurantDAO = new RestaurantDAOHibernate();
		Restaurant restaurant = restaurantDAO.searchRestaurantsbyName("Paul's Place");
		Assert.assertEquals(2,restaurant.getIdRestaurant());
	}
	
	public void testGetAllRestaurant(){
		RestaurantDAOHibernate restaurantDAO = new RestaurantDAOHibernate();
		ArrayList<Restaurant> restaurants = (ArrayList<Restaurant>) restaurantDAO.getAllRestaurants();
		Assert.assertEquals(25, restaurants.size());
	}
}
