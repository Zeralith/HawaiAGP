package test.dao;

import java.util.ArrayList;

import junit.framework.TestCase;
import persistence.data.Island;
import persistence.data.TouristAttraction;
import persistence.hibernate.IslandDAOHibernate;
import persistence.hibernate.TouristAttractionDAOHibernate;

public class IslandDAOTest extends TestCase {
	
	private IslandDAOHibernate islandDAOHib;
	
	public static void main(String[] args){
		junit.textui.TestRunner.run(IslandDAOTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
        //Data must Be loaded
        islandDAOHib =	new IslandDAOHibernate();
        TouristAttractionDAOHibernate  touristAttractionDAO = new TouristAttractionDAOHibernate();
        ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
        touristAttractions.add(touristAttractionDAO.searchTouristAttractionbyId(5));
        touristAttractions.add(touristAttractionDAO.searchTouristAttractionbyId(6));
        touristAttractions.add(touristAttractionDAO.searchTouristAttractionbyId(7));
        touristAttractions.add(touristAttractionDAO.searchTouristAttractionbyId(8));
        //islandDAOHib.createIsland(new Island(1, "island1",touristAttractions));
	}
	
	protected void tearDown() throws Exception {
        super.tearDown();
        //islandDAOHib.dropIsland("island1");
	}
	
	
	public void testGetAllIsland(){
		ArrayList<Island> islands = (ArrayList<Island>) islandDAOHib.getAllIsland();
		assertEquals(5, islands.size());
	}
	
	/*public void testGetRestaurants(){
		ArrayList<Restaurant> restaurants = (ArrayList<Restaurant>) islandDAOHib.getRestaurantsbyIsland("island1");
		assertEquals(0, restaurants.size());
	}*/
	
	public void testsearchIslandbyHotel(){
		Island island = IslandDAOHibernate.getInstance().searchIslandByHotel(7);
		System.out.println(island.getNameIsland());
	}
	
	public void testTouristAttractionByIslandAndType(){
		ArrayList<TouristAttraction> ta = (ArrayList<TouristAttraction>)IslandDAOHibernate.getInstance().getTouristAttractionsbyIslandAnDType("ile Kauai", "Historique");
		for (TouristAttraction tourist : ta){
			System.out.println(tourist.getNameAttraction() + "-"+ tourist.getTypeAttraction());
		}
	}
	
	/*public void testGetTouristAttractionsbyIsland(){
		ArrayList<TouristAttraction> touristAttractions = (ArrayList<TouristAttraction>) islandDAOHib.getTouristAttractionsbyIsland("island1");
		assertEquals(4, touristAttractions.size());
	}*/
}
