package test.dao;


import junit.framework.TestCase;
import persistence.data.Island;
import persistence.hibernate.IslandDAOHibernate;

public class HubDAOHibernateTest extends TestCase {
	
	public static void main(String[] args){
		junit.textui.TestRunner.run(HubDAOHibernateTest.class);
	}
	
	protected void setUp() throws Exception {
        super.setUp();
	}
	
	protected void tearDown() throws Exception {
        super.tearDown();
        //islandDAOHib.dropIsland("island1");
	}
	
	public void testsearchIslandbyeHubbyIsland(){
		IslandDAOHibernate islandDAOHib = new IslandDAOHibernate();
		Island island = islandDAOHib.searchIslandbyHub(1);
		assertEquals("ile Hawaii", island.getNameIsland());
	}
	

}
