package persistence.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Line {
	
	@Id
	@GeneratedValue
	private int idLine;
	private String companyLine;
	private String typeTransport;
	private double priceTransport;
	private double speedAverage;
	
	public Line() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Line (int idLine,String companyLine,String typeTransport,double priceTransport,double speedAverage){
		this.idLine=idLine;
		this.companyLine = companyLine;
		this.typeTransport=typeTransport;
		this.priceTransport=priceTransport;
		this.speedAverage=speedAverage;
	}
	
	public int getIdLine() {
		return idLine;
	}

	public void setIdLine(int idLine) {
		this.idLine = idLine;
	}

	public String getCompanyLine() {
		return companyLine;
	}

	public void setCompanyLine(String companyLine) {
		this.companyLine = companyLine;
	}

	public String getTypeTransport() {
		return typeTransport;
	}

	public void setTypeTransport(String typeTransport) {
		this.typeTransport = typeTransport;
	}

	public double getPriceTransport() {
		return priceTransport;
	}

	public void setPriceTransport(double priceTransport) {
		this.priceTransport = priceTransport;
	}

	public double getSpeedAverage() {
		return speedAverage;
	}

	public void setSpeedAverage(double speedAverage) {
		this.speedAverage = speedAverage;
	}

}
