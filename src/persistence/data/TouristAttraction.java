package persistence.data;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


import org.hibernate.annotations.Type;


@Entity
public class TouristAttraction {
	
	@Id
	@GeneratedValue
	private int idAttraction;
	@Column(name = "nameAttraction")
	private String nameAttraction;
	private String photolinkAttraction;
	private String typeAttraction;
	private double priceAttraction;
	@Type(type="text")
	private String description;
	private String coordGPS;
	private double durationVisit;

	public TouristAttraction() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TouristAttraction( int idAttraction,String nameAttraction,String photoLinkAttraction,
			String typeAttraction,double priceAttraction,String description,String coordGPS,
			double durationVisit){
		
		this.idAttraction=idAttraction;
		this.nameAttraction=nameAttraction;
		this.photolinkAttraction=photoLinkAttraction;
		this.typeAttraction=typeAttraction;
		this.priceAttraction=priceAttraction;
		this.description=description;
		this.coordGPS=coordGPS;
		this.durationVisit=durationVisit;
	}

	public int getIdAttraction() {
		return idAttraction;
	}

	public void setIdAttraction(int idAttraction) {
		this.idAttraction = idAttraction;
	}

	public String getNameAttraction() {
		return nameAttraction;
	}

	public void setNameAttraction(String nameAttraction) {
		this.nameAttraction = nameAttraction;
	}


	public String getPhotolinkAttraction() {
		return photolinkAttraction;
	}

	public void setPhotolinkAttraction(String photolinkAttraction) {
		this.photolinkAttraction = photolinkAttraction;
	}

	public String getTypeAttraction() {
		return typeAttraction;
	}

	public void setTypeAttraction(String typeAttraction) {
		this.typeAttraction = typeAttraction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public double getPriceAttraction() {
		return priceAttraction;
	}

	public void setPriceAttraction(double priceAttraction) {
		this.priceAttraction = priceAttraction;
	}

	public double getDurationVisit() {
		return durationVisit;
	}

	public void setDurationVisit(double durationVisit) {
		this.durationVisit = durationVisit;
	}
	public String getCoordGPS() {
		return coordGPS;
	}
	public void setCoordGPS(String coordGPS) {
		this.coordGPS = coordGPS;
	}

}
