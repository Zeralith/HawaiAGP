package persistence.data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Hub {

	@Id
	@GeneratedValue
	private int idHub;
	private String nameHub;
	private String coord;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Line.class)
	@JoinColumn(name = "idLine", nullable = false)
	private Line line;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Island.class)
	@JoinColumn(name = "idIsland", nullable = false)
	private Island island;
	
	public Hub() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Hub(Line line,Island island,int idHub,String nameHub, String coord){
		this.idHub=idHub;
		this.coord=coord;
		this.line=line;
		this.nameHub=nameHub;
		
		this.island=island;
	}


	public int getIdHub() {
		return idHub;
	}


	public void setIdHub(int idHub) {
		this.idHub = idHub;
	}


	public String getCoord() {
		return coord;
	}


	public void setCoord(String coord) {
		this.coord = coord;
	}

	public String getNameHub() {
		return nameHub;
	}

	public void setNameHub(String nameHub) {
		this.nameHub = nameHub;
	}

	public Line getLine() {
		return line;
	}

	public void setLine(Line line) {
		this.line = line;
	}

	public Island getIsland() {
		return island;
	}

	public void setIsland(Island island) {
		this.island = island;
	}

}
