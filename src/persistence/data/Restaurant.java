package persistence.data;

import javax.persistence.CascadeType
;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class Restaurant {
	
	@Id
	@GeneratedValue
	private int idRestaurant;
	private String nameRestaurant;
	private String addressRestaurant;
	private double priceRestaurant;
	private String typeRestaurant;
	private int stars;
	private String photoLinkRestaurant;
	private String coordGPS;
	
	@OneToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = TouristAttraction.class)
	private TouristAttraction touristAttraction;
	
	
	public Restaurant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Restaurant(int idRestaurant,String nameRestaurant,String addressRestaurant,double priceRestaurant,
			String typeRestaurant,int stars,String photoLinkRestaurant,TouristAttraction touristeAttraction){
		this.idRestaurant=idRestaurant;
		this.nameRestaurant=nameRestaurant;
		this.addressRestaurant=addressRestaurant;
		this.priceRestaurant=priceRestaurant;
		this.typeRestaurant=typeRestaurant;
		this.stars=stars;
		this.photoLinkRestaurant=photoLinkRestaurant;
		this.touristAttraction=touristeAttraction;
	}
	public Restaurant(int idRestaurant,String nameRestaurant,String addressRestaurant,double priceRestaurant,
			String typeRestaurant,int stars,String photoLinkRestaurant,String coordGPS){
		this.idRestaurant=idRestaurant;
		this.nameRestaurant=nameRestaurant;
		this.addressRestaurant=addressRestaurant;
		this.priceRestaurant=priceRestaurant;
		this.typeRestaurant=typeRestaurant;
		this.stars=stars;
		this.photoLinkRestaurant=photoLinkRestaurant;
		this.coordGPS=coordGPS;
	}
	
	public int getIdRestaurant() {
		return idRestaurant;
	}
	public void setIdRestaurant(int idRestaurant) {
		this.idRestaurant = idRestaurant;
	}
	public String getNameRestaurant() {
		return nameRestaurant;
	}
	public void setNameRestaurant(String nameRestaurant) {
		this.nameRestaurant = nameRestaurant;
	}
	public String getAdressRestaurant() {
		return addressRestaurant;
	}
	public void setAdressRestaurant(String adressRestaurant) {
		this.addressRestaurant = adressRestaurant;
	}
	public double getPrice() {
		return priceRestaurant;
	}
	public void setPrice(double price) {
		this.priceRestaurant = price;
	}
	public String getTypeRestaurant() {
		return typeRestaurant;
	}
	public void setTypeRestaurant(String typeRestaurant) {
		this.typeRestaurant = typeRestaurant;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	public String getPhotoLink() {
		return photoLinkRestaurant;
	}
	public void setPhotoLink(String photoLink) {
		this.photoLinkRestaurant = photoLink;
	}
	public String getCoordGPS() {
		return coordGPS;
	}
	public void setCoordGPS(String coordGPS) {
		this.coordGPS = coordGPS;
	}

	public TouristAttraction getTouristAttraction() {
		return touristAttraction;
	}
	public void setTouristAttraction(TouristAttraction touristAttraction) {
		this.touristAttraction = touristAttraction;
	}

}
