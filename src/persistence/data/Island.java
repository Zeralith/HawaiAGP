package persistence.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Island {
	@Id
	@GeneratedValue
	private int idIsland;
	private String nameIsland;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,targetEntity = TouristAttraction.class)
	private List<TouristAttraction> touristAttraction;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Hotel.class)
	private List<Hotel> hotel;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Restaurant.class)
	private List<Restaurant> restaurant;
	
	public Island() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Island(int idIsland,String nameIsland, ArrayList<TouristAttraction> touristAttraction){
		this.idIsland=idIsland;
		this.nameIsland=nameIsland;
		this.touristAttraction = touristAttraction;
	}

	public Island(int idIsland,String nameIsland,ArrayList<TouristAttraction> touristAttraction,
			ArrayList<Hotel> hotel,ArrayList<Restaurant> restaurant){
		this.idIsland=idIsland;
		this.nameIsland=nameIsland;
		this.touristAttraction=touristAttraction;
		this.hotel=hotel;
		this.restaurant=restaurant;
	}
	
	public int getIdIsland() {
		return idIsland;
	}
	public void setIdIsland(int idIsland) {
		this.idIsland = idIsland;
	}
	public String getNameIsland() {
		return nameIsland;
	}
	public void setNameIsland(String nameIsland) {
		this.nameIsland = nameIsland;
	}
	public List<TouristAttraction> getTouristAttraction() {
		return touristAttraction;
	}

	public void setTouristAttraction(List<TouristAttraction> touristAttraction) {
		this.touristAttraction = touristAttraction;
	}

	public List<Hotel> getHotel() {
		return hotel;
	}

	public void setHotel(List<Hotel> hotel) {
		this.hotel = hotel;
	}

	public List<Restaurant> getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(List<Restaurant> restaurant) {
		this.restaurant = restaurant;
	}
}
