package persistence.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Hotel {
	@Id
	@GeneratedValue
	private int idHotel;
	private String nameHotel;
	private String addressHotel;
	private double priceHotel;
	private int stars;
	private String gpsCoordinates;
	private String photolinkHotel;

	
	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Hotel(int idHotel, String nameHotel,String adressHotel,double priceHotel,int stars,String gpsCoordinates,String photolinHotel){
		this.idHotel = idHotel;
		this.nameHotel = nameHotel;
		this.addressHotel = adressHotel;
		this.priceHotel = priceHotel;
		this.stars = stars;
		this.gpsCoordinates = gpsCoordinates;
		this.photolinkHotel=photolinHotel;
	}
	public int getId() {
		return idHotel;
	}
	public void setId(int id) {
		this.idHotel = id;
	}
	public String getNameHotel() {
		return nameHotel;
	}
	public void setNameHotel(String nameHotel) {
		this.nameHotel = nameHotel;
	}
	public String getAdressHotel() {
		return addressHotel;
	}
	public void setAdressHotel(String adressHotel) {
		this.addressHotel = adressHotel;
	}
	public double getPriceHotel() {
		return priceHotel;
	}
	public void setPriceHotel(double priceHotel) {
		this.priceHotel = priceHotel;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public String getGpsCoordinates() {
		return gpsCoordinates;
	}

	public void setGpsCoordinates(String gpsCoordinates) {
		this.gpsCoordinates = gpsCoordinates;
	}
	public String getPhotolinkHotel() {
		return photolinkHotel;
	}
	public void setPhotolinkHotel(String photolinkHotel) {
		this.photolinkHotel = photolinkHotel;
	}

}
