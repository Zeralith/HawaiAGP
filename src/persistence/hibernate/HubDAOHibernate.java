package persistence.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import dao.HubDAO;
import persistence.data.Hub;

@SuppressWarnings("rawtypes")
public class HubDAOHibernate implements HubDAO {
	
private static HubDAOHibernate INSTANCE = null;
	
	public static HubDAOHibernate getInstance(){
		if(INSTANCE == null){ INSTANCE = new HubDAOHibernate();}
		return INSTANCE;
	}

	public void createHub(Hub newHub) {
		Session session = HibernateConnection.getSession();
		Transaction persistTransaction = session.beginTransaction();
		Hub hub = newHub ;
		session.save(hub);
		persistTransaction.commit();
		session.close();		
		
	}

	
	public void dropHub() {
		// TODO Auto-generated method stub
		
	}

	
	public List<Hub> getAllHub() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session.createQuery("Select hub from Hub hub");
		@SuppressWarnings("unchecked")
		List<Hub> result = readQuery.list();
		readTransaction.commit();
		session.close();
		return result;
	}

	
	public Hub searchHubbyId(int idHub) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Hub as hub where hub.idHub=:idHub");
		readQuery.setInteger("idHub", idHub);
		List result = readQuery.list();
		Hub hub = (Hub) result.get(0);
		readTransaction.commit();
		session.close();
		return hub;
	}

	
	public Hub searchHubbyType(String nameHub) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Hub as hub where hub.nameHub=:nameHub");
		readQuery.setString("nameHub", nameHub);
		List result = readQuery.list();
		Hub hub = (Hub) result.get(0);
		readTransaction.commit();
		session.close();
		return hub;
	}
	public List<Hub> searchHubByLine(int idLine){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("select hub from Hub as hub left join hub.line as line where line.idLine=:idLine");
		readQuery.setInteger("idLine", idLine);
		@SuppressWarnings("unchecked")
		List<Hub> hub = readQuery.list();
		readTransaction.commit();
		session.close();
		return hub;
		
	}
	

}