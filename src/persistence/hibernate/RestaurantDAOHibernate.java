package persistence.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.RestaurantDAO;
import persistence.data.Restaurant;

@SuppressWarnings("rawtypes")
public class RestaurantDAOHibernate implements RestaurantDAO {

private static RestaurantDAOHibernate INSTANCE = null;
	
	public static RestaurantDAOHibernate getInstance(){
		if(INSTANCE == null){ INSTANCE = new RestaurantDAOHibernate();}
		return INSTANCE;
	}
	
	public void createRestaurant(Restaurant newRestaurant) {
		Session session = HibernateConnection.getSession();
		Transaction persistTransaction = session.beginTransaction();
		Restaurant restaurant = newRestaurant ;
		session.save(restaurant);
		persistTransaction.commit();
		session.close();
		
	}
	
	public void dropRestaurant(String nameRestaurant) {
		// TODO Auto-generated method stub
		
	}

	
	public List<Restaurant> getAllRestaurants() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session.createQuery("Select restaurant from Restaurant restaurant");
		@SuppressWarnings("unchecked")
		List<Restaurant> result = readQuery.list();
		readTransaction.commit();
		session.close();
		return result;
	}

	
	public Restaurant searchRestaurantsbyId(int idRestaurant) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Restaurant as restaurant where restaurant.idRestaurant=:idRestaurant");
		readQuery.setInteger("idRestaurant", idRestaurant);
		List result = readQuery.list();
		Restaurant restaurant = (Restaurant) result.get(0);
		readTransaction.commit();
		session.close();
		return restaurant;
	}

	
	public Restaurant searchRestaurantsbyName(String nameRestaurant) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Restaurant as restaurant where restaurant.nameRestaurant=:nameRestaurant");
		readQuery.setString("nameRestaurant", nameRestaurant);
		List result = readQuery.list();
		Restaurant restaurant = (Restaurant) result.get(0);
		readTransaction.commit();
		session.close();
		return restaurant;
	}
	public Restaurant searchRestaurantsbyType(String typeRestaurant) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Restaurant as restaurant where restaurant.typeRestaurant=:typeRestaurant");
		readQuery.setString("typeRestaurant", typeRestaurant);
		List result = readQuery.list();
		Restaurant restaurant = (Restaurant) result.get(0);
		readTransaction.commit();
		session.close();
		return restaurant;
	}
	
	public Restaurant searchRestaurantsbyStars(int stars) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Restaurant as restaurant where restaurant.stars=:stars");
		readQuery.setInteger("typeRestaurant", stars);
		List result = readQuery.list();
		Restaurant restaurant = (Restaurant) result.get(0);
		readTransaction.commit();
		session.close();
		return restaurant;
	}
	


}
