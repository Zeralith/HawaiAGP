package persistence.hibernate;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.TouristAttractionDAO;
import persistence.data.TouristAttraction;

@SuppressWarnings("rawtypes")
public class TouristAttractionDAOHibernate implements TouristAttractionDAO {
	
	private static TouristAttractionDAOHibernate INSTANCE = null;
	
	public static TouristAttractionDAOHibernate getInstance(){
		if(INSTANCE == null){ INSTANCE = new TouristAttractionDAOHibernate();}
		return INSTANCE;
	}

	public void createTouristAttraction(TouristAttraction newTouristAttraction) {
		Session session = HibernateConnection.getSession();
		Transaction persistTransaction = session.beginTransaction();
		TouristAttraction touristAttraction = newTouristAttraction ;
		session.save(touristAttraction);
		persistTransaction.commit();
		session.close();
		
	}

	public void dropTouristAttraction() {
		// TODO Auto-generated method stub
		
	}

	public List<TouristAttraction> getAllTouristAttraction() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session.createQuery("Select touristAttraction from TouristAttraction touristAttraction");
		@SuppressWarnings("unchecked")
		List<TouristAttraction> result = readQuery.list();
		readTransaction.commit();
		session.close();
		return result;
	}

	public TouristAttraction searchTouristAttractionbyId(int idAttraction) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from TouristAttraction as touristAttraction where touristAttraction.idAttraction=:idAttraction");
		readQuery.setInteger("idAttraction", idAttraction);
		List result = readQuery.list();
		TouristAttraction touristAttraction = (TouristAttraction) result.get(0);
		readTransaction.commit();
		session.close();
		return touristAttraction;
	}

	public TouristAttraction searchTouristAttractionbyName(String nameAttraction) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from TouristAttraction as touristAttraction where touristAttraction.nameAttraction=:nameAttraction");
		readQuery.setString("nameAttraction", nameAttraction);
		List result = readQuery.list();
		TouristAttraction touristAttraction = (TouristAttraction) result.get(0);
		readTransaction.commit();
		session.close();
		return touristAttraction;
	}
	
	@SuppressWarnings("unchecked")
	public List<TouristAttraction> searchTouristAttractionbyType(String typeAttraction) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from TouristAttraction as touristAttraction where touristAttraction.typeAttraction=:typeAttraction");
		readQuery.setString("typeAttraction", typeAttraction);
		List touristAttraction = readQuery.list();
		readTransaction.commit();
		session.close();
		return touristAttraction;
	}
	
	/*Liste idAttraction*/
	public List<Integer> allIdTouristAttraction() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("select touristAttraction.idAttraction from TouristAttraction as touristAttraction");
		
		List result = readQuery.list();
		@SuppressWarnings("unchecked")
		List<Integer> touristAttraction= (List<Integer>)result;

		readTransaction.commit();
		session.close();
		return touristAttraction;
	}
	
}
