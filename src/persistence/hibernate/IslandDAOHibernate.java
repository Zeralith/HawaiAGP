package persistence.hibernate;

import java.util.ArrayList;


import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.IslandDAO;
import persistence.data.Hotel;
import persistence.data.Island;
import persistence.data.Restaurant;
import persistence.data.TouristAttraction;

@SuppressWarnings("rawtypes")
public class IslandDAOHibernate implements IslandDAO {
	
	private static IslandDAOHibernate INSTANCE = null;
	
	public static IslandDAOHibernate getInstance(){
		if(INSTANCE == null){ INSTANCE = new IslandDAOHibernate();}
		return INSTANCE;
	}


	
	public void createIsland(Island newIsland) {
		Session session = HibernateConnection.getSession();
		Transaction persistTransaction = session.beginTransaction();
		Island island = newIsland ;
		session.save(island);
		persistTransaction.commit();
		session.close();
		
	}

	
	public void dropIsland(String nameIsland) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Island as island where island.nameIsland=:nameIsland ");
		readQuery.setString("nameIsland", nameIsland);
		List result = readQuery.list();
		Island island = (Island) result.get(0);

		session.delete(island);

		readTransaction.commit();
		session.close();
		
	}

	
	public List<Island> getAllIsland() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session.createQuery("Select island from Island island");
		@SuppressWarnings("unchecked")
		List<Island> result = readQuery.list();
		readTransaction.commit();
		session.close();
		return result;
	}

	
	public Island searchIslandbyId(int idIsland) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Island as island where island.idIsland=:idIsland");
		readQuery.setInteger("idIsland", idIsland);
		List result = readQuery.list();
		Island island = (Island) result.get(0);
		readTransaction.commit();
		session.close();
		return island;
	}

	
	public Island searchIslandbyName(String nameIsland) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Island as island where island.nameIsland=:nameIsland");
		readQuery.setString("nameIsland", nameIsland);
		List result = readQuery.list();
		Island island = (Island) result.get(0);
		readTransaction.commit();
		session.close();
		return island;
	}
	
	public List<Restaurant> getRestaurantsbyIsland(String nameIsland){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		
		Query readQuery = session
				.createQuery("from Island as island where island.nameIsland=:nameIsland");
		readQuery.setString("nameIsland", nameIsland);
		List result = readQuery.list();
		
		Island island = (Island) result.get(0);
		
		List <Restaurant> restaurants = new ArrayList<Restaurant>();

		restaurants.addAll(island.getRestaurant());
		
		readTransaction.commit();
		session.close();
		
		return restaurants;	
	}
	
	public List<TouristAttraction> getTouristAttractionsbyIsland(String nameIsland){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		
		Query readQuery = session
				.createQuery("from Island as island where island.nameIsland=:nameIsland");
		readQuery.setString("nameIsland", nameIsland);
		List result = readQuery.list();
		
		Island island = (Island) result.get(0);
		
		List <TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();

		touristAttractions.addAll(island.getTouristAttraction());
		
		readTransaction.commit();
		session.close();
		
		return touristAttractions;
	}
	
	public List<TouristAttraction> getTouristAttractionsbyIslandAnDType(String nameIsland, String typeAttraction){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		
		Query readQuery = session
				.createQuery("select ta from Island as island join island.touristAttraction as ta "
						+ "where ta.typeAttraction=:typeAttraction and island.nameIsland=:nameIsland");
		readQuery.setString("nameIsland", nameIsland);
		readQuery.setString("typeAttraction", typeAttraction);
		
		List result = readQuery.list();
		
		@SuppressWarnings("unchecked")
		List <TouristAttraction> touristAttractions = (List<TouristAttraction>) result;
		
		readTransaction.commit();
		session.close();
		
		return touristAttractions;
	}
	public List<Hotel> getHotelbyIsland(String nameIsland){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		
		Query readQuery = session
				.createQuery("from Island as island where island.nameIsland=:nameIsland");
		readQuery.setString("nameIsland", nameIsland);
		List result = readQuery.list();
		
		Island island = (Island) result.get(0);
		
		List <Hotel> hotels = new ArrayList<Hotel>();

		hotels.addAll(island.getHotel());
		
		readTransaction.commit();
		session.close();
		
		return hotels;
	}
	
	public Island searchIslandbyTouristAttraction(int idTouristAttraction) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		
		Query readQuery = session
				.createQuery("select island from Island as island join island.touristAttraction as ta where ta.idAttraction=:idTouristAttraction");
		readQuery.setInteger("idTouristAttraction", idTouristAttraction);
		List result = readQuery.list();
		Island island = (Island) result.get(0);
		readTransaction.commit();
		session.close();
		
		return island;
	}
	
	public Island searchIslandbyRestaurant(int idRestaurant) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		
		Query readQuery = session
				.createQuery("select island from Island as island join island.restaurant as rest where rest.idRestaurant=:idRestaurant");
		readQuery.setInteger("idRestaurant", idRestaurant);
		List result = readQuery.list();
		Island island = (Island) result.get(0);
		readTransaction.commit();
		session.close();
		
		return island;
	}
	public Island searchIslandbyHub(int idHub){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("select island from Island as island where island = (select hub.island from Hub as hub where hub.idHub=:idHub)");
		readQuery.setInteger("idHub", idHub);
		List result = readQuery.list();
		Island island = (Island) result.get(0);
		readTransaction.commit();
		session.close();
		return island;
	}
	public Island searchIslandByHotel(int idHotel){
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("select island from Island as island join island.hotel as hot where hot.idHotel=:idHotel");
		readQuery.setInteger("idHotel", idHotel);
		List result = readQuery.list();
		Island island = (Island) result.get(0);
		readTransaction.commit();
		session.close();
		return island;
	}
	
}
