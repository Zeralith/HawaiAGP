package persistence.hibernate;

import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import persistence.data.*;

public class HibernateConnection {
	private static SessionFactory sessionFactory;
	private static AnnotationConfiguration config;

	public static AnnotationConfiguration getConfig() {
		if (config == null) {
			config = new AnnotationConfiguration();
			config.addAnnotatedClass(TouristAttraction.class);
			config.addAnnotatedClass(Restaurant.class);
			config.addAnnotatedClass(Island.class);
			config.addAnnotatedClass(Hotel.class);
			config.addAnnotatedClass(Hub.class);
			config.addAnnotatedClass(Line.class);
			
			
			
			
			
			config.configure("persistence/hibernate/connection.cfg.xml");
		}
		return config;
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				AnnotationConfiguration config = getConfig();
				sessionFactory = config.buildSessionFactory();
			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
		}
		return sessionFactory;
	}

	public static Session getSession() {
		return getSessionFactory().openSession();
	}
}
