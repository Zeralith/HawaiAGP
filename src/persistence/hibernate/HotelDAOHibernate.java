package persistence.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import dao.HotelDAO;
import persistence.data.Hotel;


public class HotelDAOHibernate implements HotelDAO {

private static HotelDAOHibernate INSTANCE = null;
	
	public static HotelDAOHibernate getInstance(){
		if(INSTANCE == null){ INSTANCE = new HotelDAOHibernate();}
		return INSTANCE;
	}

	public void createHotel(Hotel newHotel) {
		Session session = HibernateConnection.getSession();
		Transaction persistTransaction = session.beginTransaction();
		Hotel hotel = newHotel ;
		session.save(hotel);
		persistTransaction.commit();
		session.close();
		
	}


	public void dropHotel() {
		// TODO Auto-generated method stub
		
	}


	public List<Hotel> getAllHotels() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session.createQuery("Select hotel from Hotel hotel");
		@SuppressWarnings("unchecked")
		List<Hotel> result = readQuery.list();
		readTransaction.commit();
		session.close();
		return result;
	}

	
	public Hotel searchHotelbyId(int idHotel) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Hotel as hotel where hotel.idHotel=:idHotel");
		readQuery.setInteger("idHotel", idHotel);
		@SuppressWarnings("rawtypes")
		List result = readQuery.list();
		Hotel hotel = (Hotel) result.get(0);
		readTransaction.commit();
		session.close();
		return hotel;
	}

	
	public Hotel searchHotelbyName(String nameHotel) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Hotel as hotel where hotel.nameHotel=:nameHotel");
		readQuery.setString("nameHotel", nameHotel);
		@SuppressWarnings("rawtypes")
		List result = readQuery.list();
		Hotel hotel = (Hotel) result.get(0);
		readTransaction.commit();
		session.close();
		return hotel;
	}
	
	public Hotel searchHotelbyStars(int stars) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Hotel as hotel where hotel.stars=:stars");
		readQuery.setInteger("stars", stars);
		@SuppressWarnings("rawtypes")
		List result = readQuery.list();
		Hotel hotel = (Hotel) result.get(0);
		readTransaction.commit();
		session.close();
		return hotel;
	}
	
}
