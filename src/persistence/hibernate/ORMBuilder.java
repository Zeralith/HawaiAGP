package persistence.hibernate;

import java.util.ArrayList;

import buisness.model.Hotel;
import buisness.model.Hub;
import buisness.model.Line;
import buisness.model.Restaurant;
import buisness.model.TouristAttraction;
import operator.OperatorMixt;

/**
 * This class constructs persistence objects from business objects and vice
 * versa.
 * 
 */
public class ORMBuilder {
	
private static ORMBuilder INSTANCE = null;
	
	public static ORMBuilder getInstance(){
		if(INSTANCE == null){ INSTANCE = new ORMBuilder();}
		return INSTANCE;
	}
	
	/* Tourist attraction data */
	public ArrayList<TouristAttraction> buildTouristAttractionData() {
		TouristAttractionDAOHibernate touristAttracDAOHib = TouristAttractionDAOHibernate.getInstance();
		IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();
		
		ArrayList<persistence.data.TouristAttraction> allTouristAttractions ;
		ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		allTouristAttractions = (ArrayList<persistence.data.TouristAttraction>) touristAttracDAOHib.getAllTouristAttraction();
		
		
		for (persistence.data.TouristAttraction touristAttracPersist : allTouristAttractions){
			touristAttractions.add(new TouristAttraction(
					touristAttracPersist.getNameAttraction(),
					touristAttracPersist.getTypeAttraction(),
					touristAttracPersist.getDescription(),
					touristAttracPersist.getPhotolinkAttraction(),
					touristAttracPersist.getDurationVisit(),
					touristAttracPersist.getCoordGPS(),
					islandDAO.searchIslandbyTouristAttraction(touristAttracPersist.getIdAttraction()).getNameIsland(),
					touristAttracPersist.getPriceAttraction()));
			
		}
		return touristAttractions;
		
	}
	
	/* Restaurant data */
	public ArrayList<Restaurant> buildResturantData() {
		
		RestaurantDAOHibernate restaurantDAOHib = RestaurantDAOHibernate.getInstance();
		
		ArrayList<persistence.data.Restaurant> allRestaurants ;
		ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
		allRestaurants = (ArrayList<persistence.data.Restaurant>) restaurantDAOHib.getAllRestaurants();
		
		for (persistence.data.Restaurant restaurantPersist : allRestaurants){
			Restaurant restaurant = new Restaurant(
					restaurantPersist.getNameRestaurant(),
					restaurantPersist.getAdressRestaurant(),
					restaurantPersist.getPrice(),
					restaurantPersist.getTypeRestaurant(),
					restaurantPersist.getStars(),
					null,
					restaurantPersist.getPhotoLink(),
					restaurantPersist.getCoordGPS());
			restaurants.add(restaurant);
					
		}
		return restaurants;
	}

	/*Hotel */
	public ArrayList<Hotel> buildHotelData() {
		
	HotelDAOHibernate hotelDAOHib = HotelDAOHibernate.getInstance();
	IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();
	
	ArrayList<persistence.data.Hotel> allHotels ;
	ArrayList<Hotel> hotels = new ArrayList<Hotel>();
	allHotels = (ArrayList<persistence.data.Hotel>) hotelDAOHib.getAllHotels();
		
		for (persistence.data.Hotel hotelPersist : allHotels){
			hotels.add(new Hotel(
					hotelPersist.getNameHotel(),
					hotelPersist.getAdressHotel(),
					hotelPersist.getPriceHotel(),
					hotelPersist.getGpsCoordinates(),
					islandDAO.searchIslandByHotel(hotelPersist.getId()).getNameIsland(),
					hotelPersist.getStars(), 
					hotelPersist.getPhotolinkHotel()));
					
		}
		return hotels;
	}
	
	public ArrayList<Line> buildLineData() {
		
		LineDAOHibernate lineDAOHib = LineDAOHibernate.getInstance();
		ArrayList<persistence.data.Line> allLines ;
		ArrayList<Line> lines = new ArrayList<Line>();
		allLines = (ArrayList<persistence.data.Line>) lineDAOHib.getAllTransport();

		HubDAOHibernate hubDAO = HubDAOHibernate.getInstance();
		IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();

			for (persistence.data.Line linePersist : allLines){
				ArrayList<Hub> hubs = new ArrayList<Hub>();
				Line line =(new Line(
						linePersist.getTypeTransport(), 
						linePersist.getPriceTransport(), 
						linePersist.getCompanyLine(),
						hubs,
						linePersist.getSpeedAverage()));
				
				for (persistence.data.Hub hubPersist : hubDAO.searchHubByLine(linePersist.getIdLine())){
					hubs.add(new Hub(hubPersist.getCoord(),
							islandDAO.searchIslandbyHub(hubPersist.getIdHub()).getNameIsland(),
							hubPersist.getNameHub()));
				}
				lines.add(line);
			}
			return lines;
		}
	
	public ArrayList<TouristAttraction> searchTouristAttraction(String lucene){
		boolean loop = true;
		OperatorMixt operator = new OperatorMixt();
		
		String query = "select ta.idAttraction,ta.nameAttraction from TouristAttraction as ta with "+lucene;
		operator.init(query);
		
		Integer id;
		ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();
		
		while(loop){
			
			Object obj = operator.next();
			
			Object data = (Object) obj;
			
			if(obj == null){
				loop = false;	
			} else {
				id = (Integer) data;
				persistence.data.TouristAttraction taPersist = TouristAttractionDAOHibernate.getInstance().searchTouristAttractionbyId(id);
				touristAttractions.add(new TouristAttraction(taPersist.getNameAttraction(),
						taPersist.getTypeAttraction(),
						taPersist.getDescription(),
						taPersist.getPhotolinkAttraction(),
						taPersist.getDurationVisit(),
						taPersist.getCoordGPS(),
						islandDAO.searchIslandbyTouristAttraction(taPersist.getIdAttraction()).getNameIsland(),
						taPersist.getPriceAttraction()));
			}
		}
		
		
		return touristAttractions;
	}
	
	public ArrayList<TouristAttraction> buildCriteria(String islandName,String typeTouristAttraction,Boolean touristAttractionOnSameIsland) {
		IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();
		
		ArrayList<TouristAttraction> touristAttractions = new ArrayList<TouristAttraction>();
		
		if (touristAttractionOnSameIsland == true){
			for (persistence.data.TouristAttraction touristAttracPersist : islandDAO.getTouristAttractionsbyIslandAnDType(islandName, typeTouristAttraction)){
				touristAttractions.add(new TouristAttraction(
					touristAttracPersist.getNameAttraction(),
					touristAttracPersist.getTypeAttraction(),
					touristAttracPersist.getDescription(),
					touristAttracPersist.getPhotolinkAttraction(),
					touristAttracPersist.getDurationVisit(),
					touristAttracPersist.getCoordGPS(),
					islandDAO.searchIslandbyTouristAttraction(touristAttracPersist.getIdAttraction()).getNameIsland(),
					touristAttracPersist.getPriceAttraction()));
			
			}
			return touristAttractions;
			
		}else return touristAttractions = buildTouristAttractionData();
		
	}
	
	public ArrayList<Hotel> HotelByIsland(String nameIsland) {
	
		IslandDAOHibernate islandDAO = IslandDAOHibernate.getInstance();

		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
			
		for (persistence.data.Hotel hotelPersist : islandDAO.getHotelbyIsland(nameIsland)){
			hotels.add(new Hotel(
					hotelPersist.getNameHotel(),
					hotelPersist.getAdressHotel(),
					hotelPersist.getPriceHotel(),
					hotelPersist.getGpsCoordinates(),
					islandDAO.searchIslandByHotel(hotelPersist.getId()).getNameIsland(),
					hotelPersist.getStars()));
		}
		return hotels;
	}
}
