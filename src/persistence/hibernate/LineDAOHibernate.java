package persistence.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import dao.LineDAO;
import persistence.data.Line;


@SuppressWarnings("rawtypes")
public class LineDAOHibernate implements LineDAO {

private static LineDAOHibernate INSTANCE = null;
	
	public static LineDAOHibernate getInstance(){
		if(INSTANCE == null){ INSTANCE = new LineDAOHibernate();}
		return INSTANCE;
	}

	public void createLine(Line newLine) {
		Session session = HibernateConnection.getSession();
		Transaction persistTransaction = session.beginTransaction();
		Line line = newLine ;
		session.save(line);
		persistTransaction.commit();
		session.close();
		
	}


	public void dropLine(String typeTransport) {

		
	}


	public List<Line> getAllTransport() {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session.createQuery("Select line from Line line");
		@SuppressWarnings("unchecked")
		List<Line> result = readQuery.list();
		readTransaction.commit();
		session.close();
		return result;
	}

	
	public Line searchTransportbyId(int idLine) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Line as line where line.idLine=:idLine");
		readQuery.setInteger("idLine", idLine);
		List result = readQuery.list();
		Line line = (Line) result.get(0);
		readTransaction.commit();
		session.close();
		return line;
	}

	
	public Line searchTransportbyName(String typeTransport) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Line as line where line.typeTransport=:typeTransport");
		readQuery.setString("typeTransport", typeTransport);
		List result = readQuery.list();
		Line line = (Line) result.get(0);
		readTransaction.commit();
		session.close();
		return line;
	}
	
	public Line searchTransportbySpeedAverage(double speedAverage) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Line as line where line.speedAverage=:speedAverage");
		readQuery.setDouble("speedAverage", speedAverage);
		List result = readQuery.list();
		Line line = (Line) result.get(0);
		readTransaction.commit();
		session.close();
		return line;
	}
	
	public Line searchTransportbyPrice(double priceTransport) {
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		Query readQuery = session
				.createQuery("from Line as line where line.priceTransport=:priceTransport");
		readQuery.setDouble("priceTransport", priceTransport);
		List result = readQuery.list();
		Line line = (Line) result.get(0);
		readTransaction.commit();
		session.close();
		return line;
	}


	
	

}
