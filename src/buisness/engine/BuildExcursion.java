package buisness.engine;

import java.util.ArrayList;
import java.util.HashMap;

import buisness.model.Excursion;
import buisness.model.Hotel;
import buisness.model.Restaurant;
import buisness.model.TouristAttraction;
import buisness.model.Travel;
import persistence.hibernate.ORMBuilder;

public class BuildExcursion {
	
	private static BuildExcursion INSTANCE = null;
	
	private ArrayList<Restaurant> restaurants;
	private ArrayList<Restaurant> restaurantsTaken = new ArrayList<Restaurant>();
	 
	public static BuildExcursion getInstance(){			
		if (INSTANCE == null){ 	
			INSTANCE = new BuildExcursion();
		}
		return INSTANCE;
	}
	
	private BuildExcursion(){
		restaurants = ORMBuilder.getInstance().buildResturantData();
	}
	
	public void clearRestaurantTaken(){
		restaurantsTaken.clear();
		System.out.println("CLEAR !");
	}
	
	public Excursion createExcursion(ArrayList<TouristAttraction> touristAttractions, Hotel hotel){
		ManageTravel manageTravel = ManageTravel.getInstance();
		HashMap<Integer, Travel> travelsHashMap = new HashMap<Integer, Travel>();
		TouristAttraction previousTouristAttraction = null;
		Restaurant restaurant;
		Excursion excursion;
		
		restaurant = getNearestRestaurant(touristAttractions.get(0));
		restaurantsTaken.add(restaurant);
		
		int key = 0;
		double time = 0.0;
		
		for(TouristAttraction touristAttraction : touristAttractions){
			if(previousTouristAttraction == null){
				manageTravel.setTouristAttractionA(touristAttraction);
				manageTravel.setTouristAttractionB(null);
				manageTravel.setHotelB(hotel);
				travelsHashMap.put(key, manageTravel.travelBetweenDijkstra(hotel.getGpsCoordinatesHotel(),
					touristAttraction.getGpsCoordinatesTouristAttraction(),
					hotel.getIslandName(),
					touristAttraction.getIslandName(), true));
			}else{
				manageTravel.setTouristAttractionA(previousTouristAttraction);
				manageTravel.setTouristAttractionB(touristAttraction);
				manageTravel.setHotelB(null);
				travelsHashMap.put(key, manageTravel.travelBetweenDijkstra(previousTouristAttraction.getGpsCoordinatesTouristAttraction(),
						touristAttraction.getGpsCoordinatesTouristAttraction(),
						previousTouristAttraction.getIslandName(),
						touristAttraction.getIslandName(), false));
			}
			previousTouristAttraction = touristAttraction;
			key++;
		}
		manageTravel.setTouristAttractionA(previousTouristAttraction);
		manageTravel.setTouristAttractionB(null);
		manageTravel.setHotelB(hotel);
		
		travelsHashMap.put(key, manageTravel.travelBetweenDijkstra(previousTouristAttraction.getGpsCoordinatesTouristAttraction(),
				hotel.getGpsCoordinatesHotel(),
				previousTouristAttraction.getIslandName(),
				hotel.getIslandName(), false));
		
		for(Integer idTravel : travelsHashMap.keySet()){
			if(travelsHashMap.get(idTravel).getTouristAttractionA() != null){
				time += travelsHashMap.get(idTravel).getTouristAttractionA().getTimeVisiteTouristAttraction();
			}
			time += travelsHashMap.get(idTravel).getTravelTime();
		}
		excursion = new Excursion(travelsHashMap, restaurant, time);
		return excursion; 
	}
	
	public Restaurant getNearestRestaurant(TouristAttraction touristAttraction){
		Restaurant restaurant = null;
		double distBest = 99999.0, distTmp;
		
		for(Restaurant restoTmp : restaurants){
			distTmp = ManageTravel.getInstance().distanceBetweenGps(touristAttraction.getGpsCoordinatesTouristAttraction(),
					restoTmp.getGpsCoordinates());
			if(distBest>distTmp 
					&& restaurantsTaken.contains(restoTmp) != true){
				
				distBest = distTmp;
				restaurant = restoTmp;
			}
			
		}
		return restaurant;
	}
}