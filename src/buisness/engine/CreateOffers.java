package buisness.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;

import buisness.model.Excursion;
import buisness.model.Hotel;
import buisness.model.Offer;
import buisness.model.TouristAttraction;
import buisness.variable.GlobalVar;
import persistence.hibernate.ORMBuilder;

public class CreateOffers {
	
	private static CreateOffers INSTANCE = null;
	 
	public static CreateOffers getInstance(){			
		if (INSTANCE == null){ 	INSTANCE = new CreateOffers();	}
		return INSTANCE;
	}
		
	private CreateOffers(){
		
	}
	
	public HashMap<Integer, Offer> getOffersWithCriterias(DateTime start, DateTime end,
			boolean touristAttractionOnSameIsland, String touristAttractionType, String islandHotel, String lucene){
		
		if(start == null || end == null){
			DateTimeZone FRANCE = DateTimeZone.forID("Europe/Paris");
			start = new DateTime(2016, 10, 20, 5, 0, 0, FRANCE);
			end = new DateTime(2016, 10, 26, 13, 0, 0, FRANCE);
		}
		
		BuildExcursion buildExcursion = BuildExcursion.getInstance();
		
		HashMap<Integer, Offer> offers = new HashMap<Integer, Offer>();
		ArrayList<Hotel> hotelsMatch;
		ArrayList<TouristAttraction> touristAttractionsMatch = null;
		ArrayList<TouristAttraction> touristAttractionsTmp = new ArrayList<TouristAttraction>();
		ArrayList<TouristAttraction> touristAttractionTaken = new ArrayList<TouristAttraction>();
		ArrayList<TouristAttraction> allTouristAttractions = ORMBuilder.getInstance().buildTouristAttractionData();
		TouristAttraction ta;
		
		double timeMorning = 12 - GlobalVar.DAY_START, timeAfternoon = GlobalVar.DAY_END - 14;
		double nbDay, timeTot, sommeTmp;
		int key = 0, indexTAMATCH;
		Random rand = new Random();
		int numRand = 0;
		
		hotelsMatch = ORMBuilder.getInstance().HotelByIsland(islandHotel);
		touristAttractionsMatch = matchTouristAttractionCriteria(islandHotel, touristAttractionType, touristAttractionOnSameIsland);
		
		nbDay = Days.daysBetween(start,
                end).getDays();
		
		allTouristAttractions.removeAll(touristAttractionsMatch);
		
		for(Hotel hotel : hotelsMatch){
			HashMap<Integer, Excursion> excursions = new HashMap<Integer, Excursion>();
			sommeTmp = 0.0;
			indexTAMATCH = 0;
			for(int i=0; i<nbDay+1 ; i++){
				timeTot = timeMorning + timeAfternoon;
				if(indexTAMATCH < touristAttractionsMatch.size()){
					do{
						numRand = rand.nextInt(touristAttractionsMatch.size());
						ta = touristAttractionsMatch.get(numRand);
					}while(touristAttractionTaken.contains(ta) == true && touristAttractionTaken.isEmpty());
					touristAttractionTaken.add(ta);
					if(ta.getTimeVisiteTouristAttraction() < timeTot){
					touristAttractionsTmp.add(ta);
					timeTot -= ta.getTimeVisiteTouristAttraction();
					indexTAMATCH++;
					}
				} else{
					do{
						numRand = rand.nextInt(allTouristAttractions.size());
						ta = allTouristAttractions.get(numRand);
					}while(touristAttractionTaken.contains(ta) == true && touristAttractionTaken.isEmpty());
					touristAttractionTaken.add(ta);
					if(ta.getTimeVisiteTouristAttraction() <= timeTot){
						touristAttractionsTmp.add(ta);
						timeTot -= ta.getTimeVisiteTouristAttraction();
					}
				}
				
				if(!touristAttractionsTmp.isEmpty()){
					excursions.put(i, buildExcursion.createExcursion(touristAttractionsTmp, hotel));
					touristAttractionsTmp.clear();
				}
			}
			key++;
		
			for(Integer idExcursion : excursions.keySet()){
				for(Integer idTravel : excursions.get(idExcursion).getTravelsHashMap().keySet()){
					if(excursions.get(idExcursion).getTravelsHashMap().get(idTravel).getTouristAttractionA() != null){
						sommeTmp += excursions.get(idExcursion).getTravelsHashMap().get(idTravel).getTouristAttractionA().getPriceTouristAttraction();
					}
				}
			}
		
			sommeTmp += hotel.getPriceHotel()*nbDay;
			
			offers.put(key, new Offer(excursions, hotel, sommeTmp, start, end));
			buildExcursion.clearRestaurantTaken();
		}
		return offers;
	}
	
	public ArrayList<TouristAttraction> matchTouristAttractionCriteria(String islandHotel, String touristAttractionType,
			boolean touristAttractionOnSameIsland){
		ArrayList<TouristAttraction> touristAttractions = ORMBuilder.getInstance().buildTouristAttractionData();
		ArrayList<TouristAttraction> touristAttractionsMatchTmp = new ArrayList<TouristAttraction>();
		
		for(TouristAttraction touristAttraction : touristAttractions){
			if(touristAttractionOnSameIsland 
					&& islandHotel.equals(touristAttraction.getIslandName())
					&& touristAttractionType.equals(touristAttraction.getTypeTouristAttraction())){
				touristAttractionsMatchTmp.add(touristAttraction);
			} else if(!touristAttractionOnSameIsland 
					&& touristAttractionType.equals(touristAttraction.getTypeTouristAttraction())){
				touristAttractionsMatchTmp.add(touristAttraction);
			}
		}
		
		return touristAttractionsMatchTmp;
	}
	
	public ArrayList<Hotel> matchHotelWithCriteria(String islandName){
		ArrayList<Hotel> hotels = ORMBuilder.getInstance().buildHotelData();
		ArrayList<Hotel> hotelsMatchTmp = new ArrayList<Hotel>();
		
		for(Hotel hotel : hotels){
			if(hotel.getIslandName().equals(islandName))
				hotelsMatchTmp.add(hotel);
		}
		return hotelsMatchTmp;
	}
}
