package buisness.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import buisness.dijkstra.Graph;
import buisness.dijkstra.Graph.Vertex;
import buisness.model.Hotel;
import buisness.model.Hub;
import buisness.model.Line;
import buisness.model.TouristAttraction;
import buisness.model.Travel;
import buisness.variable.GlobalVar;
import persistence.hibernate.ORMBuilder;

public class ManageTravel {
	
	private TouristAttraction touristAttractionA = null;
	private TouristAttraction touristAttractionB = null;
	private Hotel hotelB = null;
	
	private ArrayList<Line> lines;
	private double bestDistAB, bestTimeAB;
	
	private ManageTravel(){
		lines = ORMBuilder.getInstance().buildLineData();
	}
 
	private static ManageTravel INSTANCE = null;
 
	public static ManageTravel getInstance(){			
		if (INSTANCE == null){ 	INSTANCE = new ManageTravel();	}
		return INSTANCE;
	}

	public double distanceBetweenGps(String gps1, String gps2){
		double sourceLat = 0.0;
		double sourceLong = 0.0;
		double destLat = 0.0;
		double destLong = 0.0;
		double d;
		  
		try{
			Pattern p = Pattern .compile("([^, ]*),[ ]*([^, ]*)");
			Matcher m = p.matcher(gps1);
			if(m.matches()){
			   sourceLat = Double.valueOf(m.group(1))* (Math.PI/180);
			   sourceLong = Double.valueOf(m.group(2))* (Math.PI/180);
			 }
			 m = p.matcher(gps2);
			 if(m.matches()){
			   destLat = Double.valueOf(m.group(1))* (Math.PI/180);
			   destLong = Double.valueOf(m.group(2))* (Math.PI/180);
			 }
		}catch(PatternSyntaxException pse){
		}
		  
		d = GlobalVar.R * (Math.PI/2 - Math.asin(( Math.sin(destLat) * Math.sin(sourceLat) + Math.cos(destLong - sourceLong) 
		  				* Math.cos(destLat) * Math.cos(sourceLat))));
		return d;
	}
	
	private HashMap<Integer, Hub> createTravelWithGraph(ArrayList<Graph.Edge> graphArrayList){
		Graph.Edge[] graphTab = new Graph.Edge[graphArrayList.size()];
		HashMap<Integer, Hub> tracks = new HashMap<Integer, Hub>();
		graphTab = graphArrayList.toArray(graphTab);
		Graph graph = new Graph(graphTab);
		
		Graph.Vertex v;
		graph.dijkstra("A");
		HashMap<Integer, Vertex> trackVertex = new HashMap<Integer, Graph.Vertex>();
		int key = 0;
		v = graph.getGraph().get("B");
		bestTimeAB = v.dist;
		bestDistAB = v.time;
		
		while(v.previous != null && v != v.previous){
			trackVertex.put(key, v);
			key++;
			v = v.previous;
		}
		trackVertex.put(key, graph.getGraph().get("A"));
		
		//Read in reverse track = best track
		for(int i=trackVertex.size()-1; i>-1 ; i--){
			for(Line line :lines){
				for(Hub hub : line.getHubsOnLine()){
					if(hub.getNameHub().equals(trackVertex.get(i).name)){
						tracks.put(key,hub);
						key++;
						break;
					}
				}
			}
		}
		return tracks;
	}
	
	private HashMap<Integer, TouristAttraction> createTouristWithGraph(ArrayList<Graph.Edge> graphArrayList, ArrayList<TouristAttraction> touristAttractions){
		Graph.Edge[] graphTab = new Graph.Edge[graphArrayList.size()];
		HashMap<Integer, TouristAttraction> tracks = new HashMap<Integer, TouristAttraction>();
		graphTab = graphArrayList.toArray(graphTab);
		Graph graph = new Graph(graphTab);
		
		Graph.Vertex v;
		graph.dijkstra("A");
		HashMap<Integer, Vertex> trackVertex = new HashMap<Integer, Graph.Vertex>();
		int key = 0;
		v = graph.getGraph().get("B");
		bestTimeAB = v.dist;
		bestDistAB = v.time;
		while(v.previous != null && v != v.previous){
			trackVertex.put(key, v);
			key++;
			v = v.previous;
		}
		trackVertex.put(key, graph.getGraph().get("A"));
		
		//Read in reverse track = best track
		for(int i=trackVertex.size()-1; i>-1 ; i--){
			for(TouristAttraction touristAttraction :touristAttractions){
				if(touristAttraction.getNameTouristAttraction().equals(trackVertex.get(i).name)){
					tracks.put(key,touristAttraction);
					key++;
					break;
				}
			}
		}
		return tracks;
	}

	public Travel travelBetweenDijkstra(String gpsStart, String gpsEnd, String islandStart, String islandEnd, boolean hotelToTouristAttraction){
		Travel travel = null;
		HashMap<Integer, Hub> tracks;
		
			//dist(in km) / speed(in km/h) for cost
			ArrayList<Graph.Edge> graphArrayList = new ArrayList<Graph.Edge>();
			double minDistBetweenStartAndHub, minDistbetweenEndAndHub, tmpStart, tmpEnd, tmpDist = 0.0;
			Hub minDistStartHub, minDistEndHub, previousHub;
			if(islandStart.equals(islandEnd)){
					graphArrayList.add(new Graph.Edge("A", "B", distanceBetweenGps(gpsStart, gpsEnd)/GlobalVar.BUS_SPEED,distanceBetweenGps(gpsStart, gpsEnd)));
			}
			
			for(Line line : lines){
				minDistBetweenStartAndHub = 9999.0; minDistbetweenEndAndHub = 9999.0;
				minDistStartHub = null; minDistEndHub = null; previousHub = null;
				
				for(Hub hub : line.getHubsOnLine()){
					//minDistHub of each Line
					tmpStart = distanceBetweenGps(gpsStart, hub.getGpsCoordinates());
					tmpEnd = distanceBetweenGps(gpsEnd, hub.getGpsCoordinates());
					
					if(minDistBetweenStartAndHub>tmpStart && islandStart.equals(hub.getIslandName())){
						minDistBetweenStartAndHub = tmpStart; minDistStartHub = hub;
					}
					if(minDistbetweenEndAndHub>tmpEnd && islandEnd.equals(hub.getIslandName())){
						minDistbetweenEndAndHub = tmpEnd; minDistEndHub = hub;
					}
					//Add in graph link betweenHub
					if(previousHub != null){
						tmpDist = distanceBetweenGps(hub.getGpsCoordinates(), previousHub.getGpsCoordinates());
						graphArrayList.add(new Graph.Edge(previousHub.getNameHub(), hub.getNameHub(), tmpDist/line.getSpeed(), tmpDist));
					}
					previousHub = hub;
				}
				
				//Add in graph for each hub nearest of A(gpsStart) and B(gpsEnd)
				if(minDistStartHub != null)
					graphArrayList.add(new Graph.Edge("A", minDistStartHub.getNameHub(), minDistBetweenStartAndHub/GlobalVar.BUS_SPEED, minDistBetweenStartAndHub));
				if(minDistEndHub != null)
					graphArrayList.add(new Graph.Edge(minDistEndHub.getNameHub(), "B", minDistbetweenEndAndHub/GlobalVar.BUS_SPEED, minDistbetweenEndAndHub));
			}
			
			tracks = createTravelWithGraph(graphArrayList);
			travel = new Travel(touristAttractionA, touristAttractionB, hotelB, new Double(bestDistAB), tracks, new Double(bestTimeAB), hotelToTouristAttraction);
		
		return travel;
	}
	
	public HashMap<Integer, TouristAttraction> getPathTouristAttraction(ArrayList<TouristAttraction> touristAttractions){
		HashMap<Integer, TouristAttraction> touristAttractionsMap;
		ArrayList<Graph.Edge> graphArrayList = new ArrayList<Graph.Edge>();
		TouristAttraction previous = null;
		Double distance;
		
		for(TouristAttraction touristAttraction : touristAttractions){
			if(previous != null){
				distance = distanceBetweenGps(touristAttraction.getGpsCoordinatesTouristAttraction(), previous.getGpsCoordinatesTouristAttraction());
				graphArrayList.add(new Graph.Edge(touristAttraction.getNameTouristAttraction(), previous.getNameTouristAttraction(), distance, 0.0));
			}
			previous = touristAttraction;
		}
		
		touristAttractionsMap = createTouristWithGraph(graphArrayList, touristAttractions);
		
		return touristAttractionsMap;
	}
	
	public TouristAttraction getTouristAttractionA() {
		return touristAttractionA;
	}

	public void setTouristAttractionA(TouristAttraction touristAttractionA) {
		this.touristAttractionA = touristAttractionA;
	}

	public TouristAttraction getTouristAttractionB() {
		return touristAttractionB;
	}

	public void setTouristAttractionB(TouristAttraction touristAttractionB) {
		this.touristAttractionB = touristAttractionB;
	}

	public Hotel getHotelB() {
		return hotelB;
	}

	public void setHotelB(Hotel hotelB) {
		this.hotelB = hotelB;
	}

}
