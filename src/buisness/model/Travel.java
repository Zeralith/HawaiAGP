package buisness.model;

import java.util.HashMap;

public class Travel {

	private TouristAttraction touristAttractionA;
	private TouristAttraction touristAttractionB;
	private Hotel hotelB;
	private double distance;
	private HashMap<Integer, Hub> tracks;
	private double travelTime;
	private boolean hotelToTouristAttraction;
	
	public Travel(TouristAttraction touristAttractionA, TouristAttraction touristAttractionB, Hotel hotelB,
			double distance, HashMap<Integer, Hub> tracks, double travelTime, boolean hotelToTouristAttraction) {
		this.touristAttractionA = touristAttractionA;
		this.touristAttractionB = touristAttractionB;
		this.hotelB = hotelB;
		this.distance = distance;
		this.tracks = tracks;
		this.travelTime = travelTime;
		this.hotelToTouristAttraction = hotelToTouristAttraction;
	}

	public boolean isHotelToTouristAttraction() {
		return hotelToTouristAttraction;
	}

	public double getTravelTime() {
		return travelTime;
	}
	
	public String getFormattedTravelTime() {
		double hours = travelTime;
		double hoursFin = Math.ceil(hours);
		double minFin = Math.ceil(( (hours-Math.ceil(hours))*60 ));
		
		String str = ""+ (int)hoursFin + "h " + (int)minFin  + " min";
		
		return str;
	}

	public TouristAttraction getTouristAttractionA() {
		return touristAttractionA;
	}

	public TouristAttraction getTouristAttractionB() {
		return touristAttractionB;
	}

	public Hotel getHotelB() {
		return hotelB;
	}

	public double getDistance() {
		return distance;
	}

	public HashMap<Integer, Hub> getTracks() {
		return tracks;
	}
	
	public String toString() {
		String tracksDisplay = "";
		if(!tracks.isEmpty()){
			for(int num : tracks.keySet()){
				tracksDisplay += tracks.get(num).getNameHub() + " -> ";
			}
		}
		if(hotelB == null){
			return "Travel : [ distance : " + distance + ", time : " + travelTime
				+ "\ntracks : touristAttractionA:" + touristAttractionA.getNameTouristAttraction() + " -> "
				+ tracksDisplay + "touristAttractionB:"+ touristAttractionB.getNameTouristAttraction() + "]";
		} else {
			if(hotelToTouristAttraction){
				return "Travel : [ distance : " + distance + ", time : " + travelTime + "\ntracks : touristAttractionA:" 
						+ "HotelA:"+ hotelB.getNameHotel() + " -> "
						+ tracksDisplay + touristAttractionA.getNameTouristAttraction()  +  "]";
			}
			else{
				return "Travel : [ distance : " + distance + ", time : " + travelTime + "\ntracks : touristAttractionA:" 
					+ touristAttractionA.getNameTouristAttraction()  + " -> "
					+ tracksDisplay + "HotelB:"+ hotelB.getNameHotel() + "]";
			}
		}
	}
}
