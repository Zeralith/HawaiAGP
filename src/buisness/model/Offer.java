package buisness.model;

import java.util.HashMap;

import org.joda.time.DateTime;

public class Offer {
	private HashMap<Integer, Excursion> excursionsHashMap;
	private Hotel hotel;
	private double totalPrice;
	private DateTime dateDebut;
	private DateTime dateFin;
	
	public Offer(HashMap<Integer, Excursion> excursionsHashMap, Hotel hotel, double totalPrice, DateTime dateDebut,
			DateTime dateFin) {
		this.excursionsHashMap = excursionsHashMap;
		this.hotel = hotel;
		this.totalPrice = totalPrice;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public HashMap<Integer, Excursion> getExcursionsHashMap() {
		return excursionsHashMap;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public DateTime getDateDebut() {
		return dateDebut;
	}

	public DateTime getDateFin() {
		return dateFin;
	}
	
	@Override
	public String toString() {
		return "Offer [excursionsHashMap=" + excursionsHashMap + ", hotel=" + hotel + ", totalPrice=" + totalPrice
				+ ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + "]";
	}
	
}
