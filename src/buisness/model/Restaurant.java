package buisness.model;

public class Restaurant {
	@Override
	public String toString() {
		return "Restaurant [nameRestaurant=" + nameRestaurant + ", addressRestaurant=" + addressRestaurant
				+ ", priceRestaurant=" + priceRestaurant + ", typeRestaurant=" + typeRestaurant + ", touristAttraction="
				+ touristAttraction + ", stars=" + stars + ", photoLinkRestaurant=" + photoLinkRestaurant
				+ ", gpsCoordinates=" + gpsCoordinates + "]";
	}

	private String nameRestaurant;
	private String addressRestaurant;
	private double priceRestaurant;
	private String typeRestaurant;
	private TouristAttraction touristAttraction;
	private int stars;
	private String photoLinkRestaurant;
	private String gpsCoordinates;
	
	public Restaurant(String nameRestaurant, String addressRestaurant, double priceRestaurant, String typeRestaurant, int stars,
			TouristAttraction touristAttraction, String photoLinkRestaurant, String gpsCoordinates) {
		this.nameRestaurant = nameRestaurant;
		this.addressRestaurant = addressRestaurant;
		this.priceRestaurant = priceRestaurant;
		this.typeRestaurant = typeRestaurant;
		this.stars = stars;
		this.touristAttraction = touristAttraction;
		this.photoLinkRestaurant = photoLinkRestaurant;
		this.gpsCoordinates = gpsCoordinates;
	}

	public String getGpsCoordinates() {
		return gpsCoordinates;
	}

	public TouristAttraction getTouristAttraction() {
		return touristAttraction;
	}

	public String getNameRestaurant() {
		return nameRestaurant;
	}

	public String getAddressRestaurant() {
		return addressRestaurant;
	}

	public double getPriceRestaurant() {
		return priceRestaurant;
	}

	public String getTypeRestaurant() {
		return typeRestaurant;
	}

	public int getStars() {
		return stars;
	}

	public String getPhotoLinkRestaurant() {
		return photoLinkRestaurant;
	}
	
}
