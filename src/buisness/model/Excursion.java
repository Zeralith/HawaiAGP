package buisness.model;

import java.util.HashMap;

public class Excursion {
	private HashMap<Integer, Travel> travelsHashMap;
	private Restaurant lunchRestaurant;
	private double timeInHours;

	public Excursion(HashMap<Integer, Travel> travelsHashMap, Restaurant lunchRestaurant, double timeInHours) {
		this.travelsHashMap = travelsHashMap;
		this.lunchRestaurant = lunchRestaurant;
		this.timeInHours = timeInHours;
	}

	public HashMap<Integer, Travel> getTravelsHashMap() {
		return travelsHashMap;
	}

	public Restaurant getLunchRestaurant() {
		return lunchRestaurant;
	}
	
	public double getTimeInHours() {
		return timeInHours;
	}

	@Override
	public String toString() {
		if(lunchRestaurant !=null){
			return "Excursion [ lunchRestaurant=" + lunchRestaurant + ", timeInHours="+ timeInHours 
				+ " travelsHashMap=" + travelsHashMap + "]";
		} else {
			return "Excursion [ timeInHours="+ timeInHours 
					+ " travelsHashMap=" + travelsHashMap + "]";
		}
	}
	
	public String getFormattedTime(double hours){
		double hoursFin = Math.ceil(hours);
		double minFin = Math.ceil(( (hours-Math.ceil(hours))*60 ));
		
		String str = ""+ (int)hoursFin + "h " + (int)minFin  + " min";
		
		return str;
	}
}
