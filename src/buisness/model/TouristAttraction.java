package buisness.model;

public class TouristAttraction {

	private String nameTouristAttraction;
	private String typeTouristAttraction;	
	private String descriptionTouristAttraction;
	private double timeVisiteTouristAttraction;
	private String gpsCoordinatesTouristAttraction;
	private String islandName;
	private double priceTouristAttraction;
	private String photoLink;
	
	public TouristAttraction(String nameTouristAttraction,
			String typeTouristAttraction, String descriptionTouristAttraction, String photoLink,
			Double timeVisiteTouristAttraction, String gpsCoordinatesTouristAttraction, String islandName,  double price) {
		this.nameTouristAttraction = nameTouristAttraction;
		this.typeTouristAttraction = typeTouristAttraction;
		this.descriptionTouristAttraction = descriptionTouristAttraction;
		this.gpsCoordinatesTouristAttraction = gpsCoordinatesTouristAttraction;
		this.timeVisiteTouristAttraction = timeVisiteTouristAttraction;
		this.islandName = islandName;
		this.priceTouristAttraction = price;
		this.photoLink = photoLink;
	}

	public String getPhotoLink() {
		return photoLink;
	}

	public String getIslandName() {
		return islandName;
	}

	public String getNameTouristAttraction() {
		return nameTouristAttraction;
	}

	public String getTypeTouristAttraction() {
		return typeTouristAttraction;
	}

	public String getDescriptionTouristAttraction() {
		return descriptionTouristAttraction;
	}

	public String getGpsCoordinatesTouristAttraction() {
		return gpsCoordinatesTouristAttraction;
	}
	
	public double getTimeVisiteTouristAttraction(){
		return timeVisiteTouristAttraction;
	}

	public double getPriceTouristAttraction() {
		return priceTouristAttraction;
	}
	
	@Override
	public String toString() {
		return "TouristAttraction [nameTouristAttraction=" + nameTouristAttraction + ", typeTouristAttraction="
				+ typeTouristAttraction + ", descriptionTouristAttraction=" + descriptionTouristAttraction
				+ ", timeVisiteTouristAttraction=" + timeVisiteTouristAttraction + ", gpsCoordinatesTouristAttraction="
				+ gpsCoordinatesTouristAttraction + ", islandName=" + islandName + ", priceTouristAttraction="
				+ priceTouristAttraction + ", photoLink=" + photoLink + "]";
	}
	
}
