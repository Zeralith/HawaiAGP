package buisness.model;

public class Hotel {

	private String nameHotel;
	private String addressHotel;
	private double priceHotel;
	private String gpsCoordinatesHotel;
	private String islandName;
	private int numberOfStars;
	private String hotelPhotoLink;
	
	public Hotel(String nameHotel, String addressHotel, double priceHotel, String gpsCoordinatesHotel,String islandName, int numberOfStars) {
		this.nameHotel = nameHotel;
		this.addressHotel = addressHotel;
		this.priceHotel = priceHotel;
		this.gpsCoordinatesHotel = gpsCoordinatesHotel;
		this.islandName = islandName;
		this.numberOfStars = numberOfStars;
	}
	
	public Hotel(String nameHotel, String addressHotel, double priceHotel, String gpsCoordinatesHotel,String islandName, int numberOfStars, String photoLink) {
		this.nameHotel = nameHotel;
		this.addressHotel = addressHotel;
		this.priceHotel = priceHotel;
		this.gpsCoordinatesHotel = gpsCoordinatesHotel;
		this.islandName = islandName;
		this.numberOfStars = numberOfStars;
		this.hotelPhotoLink = photoLink;
	}
 

	public String getIslandName() {
		return islandName;
	}

	public String getNameHotel() {
		return nameHotel;
	}

	public String getAddressHotel() {
		return addressHotel;
	}

	public double getPriceHotel() {
		return priceHotel;
	}

	public String getGpsCoordinatesHotel() {
		return gpsCoordinatesHotel;
	}

	public int getNumberOfStars() {
		return numberOfStars;
	}
	
	@Override
	public String toString() {
		return "Hotel [nameHotel=" + nameHotel + ", addressHotel=" + addressHotel + ", priceHotel=" + priceHotel
				+ ", gpsCoordinatesHotel=" + gpsCoordinatesHotel + ", islandName=" + islandName + ", numberOfStars="
				+ numberOfStars + "]";
	}

	public String getHotelPhotoLink() {
		return hotelPhotoLink;
	}
	
	
}
