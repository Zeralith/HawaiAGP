package buisness.model;

import java.util.ArrayList;

public class Line {

	private String typeTransport;
	private double priceTransport;
	private String companyName;
	private ArrayList<Hub> hubsOnLine;
	private double speed;
	
	public Line(String typeTransport, double priceTransport, String companyName, ArrayList<Hub> hubsOnLine,
			double speed) {
		this.typeTransport = typeTransport;
		this.priceTransport = priceTransport;
		this.companyName = companyName;
		this.hubsOnLine = hubsOnLine;
		this.speed = speed;
	}

	public double getSpeed() {
		return speed;
	}

	public ArrayList<Hub> getHubsOnLine() {
		return hubsOnLine;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getTypeTransport() {
		return typeTransport;
	}

	public double getPriceTransport() {
		return priceTransport;
	}
	
	@Override
	public String toString() {
		return "Line [typeTransport=" + typeTransport + ", priceTransport=" + priceTransport + ", companyName="
				+ companyName + ", hubsOnLine=" + hubsOnLine + ", speed=" + speed + "]";
	}
}
