package buisness.model;

public class Hub {
	private String nameHub;
	private String gpsCoordinates;
	private String islandName;

	
	public Hub(String gpsCoordinates, String islandName, String nameHub) {
		this.gpsCoordinates = gpsCoordinates;
		this.nameHub = nameHub;
		this.islandName = islandName;
	}

	public String getIslandName() {
		return islandName;
	}

	public String getNameHub() {
		return nameHub;
	}

	public String getGpsCoordinates() {
		return gpsCoordinates;
	}


	
	public String toString() {
		return "Hub [nameHub=" + nameHub + ", gpsCoordinates=" + gpsCoordinates + ", islandName=" + islandName + "]";
	}
	
}
