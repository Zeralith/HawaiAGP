package buisness.dijkstra;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;

public class Graph {
	   private final Map<String, Vertex> graph; // mapping of vertex names to Vertex objects, built from a set of Edges
	 
	   public Map<String, Vertex> getGraph() {
		return graph;
	   }
	   
	   public class Coord{
		   private double dist;
		   private double time;
		   
		   public Coord(double dist, double time){
			   this.time = time;
			   this.dist = dist;
		   }
		   
		   public double getDist(){
			   return dist;
		   }
		   
		   public double getTime(){
			   return time;
		   }
	   }
	   
	/** One edge of the graph (only used by Graph constructor) */
	   public static class Edge {
	      public final String v1, v2;
	      public final double dist;
	      public final double time;
	      public Edge(String v1, String v2, double dist, double time) {
	         this.v1 = v1;
	         this.v2 = v2;
	         this.dist = dist;
	         this.time = time;
	      }
	   }
	 
	   /** One vertex of the graph, complete with mappings to neighbouring vertices */
	   public static class Vertex implements Comparable<Vertex> {
	      public final String name;
	      public double dist = 99999.0; // MAX_VALUE assumed to be infinity
	      public double time = 99999.0;
	      public Vertex previous = null;
	      public final Map<Vertex, Coord> neighbours = new HashMap<Vertex, Coord>();
	 
	      public Vertex(String name) {
	         this.name = name;
	      }
	 
	      private void printPath() {
	         if (this == this.previous) {
	            System.out.printf("%s", this.name);
	         } else if (this.previous == null) {
	            System.out.printf("%s(unreached)", this.name);
	         } else {
	            this.previous.printPath();
	            System.out.printf(" -> %s(%.2f)", this.name, this.dist);
	         }
	      }
	 
	      public int compareTo(Vertex other) {
	    	  if(dist == other.dist)
	    		  return 0;
	    	  else if(dist < other.dist)
	    		  return -1;
	    	  else
	    		  return 1;
	      }
	   }
	 
	   /** Builds a graph from a set of edges */
	   public Graph(Edge[] edges) {
	      graph = new HashMap<String, Vertex>(edges.length);
	 
	      //one pass to find all vertices
	      for (Edge e : edges) {
	         if (!graph.containsKey(e.v1)) graph.put(e.v1, new Vertex(e.v1));
	         if (!graph.containsKey(e.v2)) graph.put(e.v2, new Vertex(e.v2));
	      }
	 
	      //another pass to set neighbouring vertices
	      for (Edge e : edges) {//TODO CHANGE HERE
	         graph.get(e.v1).neighbours.put(graph.get(e.v2), new Coord(e.dist,e.time));
	         graph.get(e.v2).neighbours.put(graph.get(e.v1), new Coord(e.dist,e.time)); // also do this for an undirected graph
	      }
	   }
	 
	   /** Runs dijkstra using a specified source vertex */ 
	   public void dijkstra(String startName) {
	      if (!graph.containsKey(startName)) {
	         System.err.printf("Graph doesn't contain start vertex \"%s\"\n", startName);
	         return;
	      }
	      final Vertex source = graph.get(startName);
	      NavigableSet<Vertex> q = new TreeSet<Vertex>();
	 
	      // set-up vertices
	      for (Vertex v : graph.values()) {
	         v.previous = v == source ? source : null;
	         v.dist = v == source ? 0 : 99999.0;
	         v.time = v == source ? 0 : 99999.0;
	         q.add(v);
	      }
	 
	      dijkstra(q);
	   }
	 
	   /** Implementation of dijkstra's algorithm using a binary heap. */
	   private void dijkstra(final NavigableSet<Vertex> q) {      
	      Vertex u, v;
	      while (!q.isEmpty()) {
	 
	         u = q.pollFirst(); // vertex with shortest distance (first iteration will return source)
	         if (u.dist == 99999.0) break;
	 
	         //look at distances to each neighbour
	         for (Map.Entry<Vertex, Coord> a : u.neighbours.entrySet()) {
	            v = a.getKey(); //the neighbour in this iteration
	 
	            final double alternateDist = u.dist + a.getValue().getDist();
	            final double alternateTime = u.time + a.getValue().getTime();
	            if (alternateDist < v.dist) { // shorter path to neighbour found
	               q.remove(v);
	               v.dist = alternateDist;
	               v.time = alternateTime;
	               v.previous = u;
	               q.add(v);
	            } 
	         }
	      }
	   }
	 
	   /** Prints a path from the source to the specified vertex */
	   public void printPath(String endName) {
	      if (!graph.containsKey(endName)) {
	         System.err.printf("Graph doesn't contain end vertex \"%s\"\n", endName);
	         return;
	      }
	 
	      graph.get(endName).printPath();
	      System.out.println();
	   }
	   /** Prints the path from the source to every vertex (output order is not guaranteed) */
	   public void printAllPaths() {
	      for (Vertex v : graph.values()) {
	         v.printPath();
	         System.out.println();
	      }
	   }
	}