package operator;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import persistence.data.TouristAttraction;
import persistence.hibernate.HibernateConnection;

public class OperatorSQL implements OperatorInterface{
	
	private int index;
	private List <TouristAttraction> result;
	private static OperatorSQL INSTANCE = null;
	
	public OperatorSQL getInstance(){
		if(INSTANCE == null){ INSTANCE = new OperatorSQL();}
		return INSTANCE;
	}
	
	@SuppressWarnings("unchecked")
	public void init(String query) 
	{
		long startTime = System.currentTimeMillis();
		Session session = HibernateConnection.getSession();
		Transaction readTransaction = session.beginTransaction();
		//System.out.println(query);
		Query readQuery = session.createQuery(query);
		result = readQuery.list();
		index= 0;
		readTransaction.commit();
		session.close();
		long endTime = System.currentTimeMillis();
		System.out.println("SQL, time taken: "
				+(endTime-startTime)+" ms");

	}

	public Object next() {
		index++;
		if(index > result.size())
			return null;
	//System.out.println(((TouristAttraction)result.get(index-1)).getNameAttraction());
		return result.get(index-1);
	}
}
