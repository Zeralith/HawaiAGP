package operator;
 
 import java.util.ArrayList;
 import java.util.List;
 
 public class OperatorMixt implements OperatorInterface
 {
 	OperatorSQL osql;
 	OperatorLucene oluc;
 	String querySQL;
 	String queryLucene;
 	List<Object> result;
 	int index;
 	
 	public void init(String query) 
 	{
 		boolean loop1 = true;
 		boolean loop2 = true;
 		String list[] = query.split("with");
 		
 		querySQL = list[0].trim();
 		queryLucene = list[1].trim();
 		
 		osql = new OperatorSQL();
 		osql.init(querySQL);
 		oluc = new OperatorLucene();
 		oluc.init(queryLucene);
 		
 		result = new ArrayList<Object>();
 		List<Object> resultsql = new ArrayList<Object>();
 
 		while(loop2)
 		{
 			Object obj = osql.next();
 			Object[] data = (Object[]) obj;
 			
 			if(obj==null)
 				loop2 = false;
 			else
 				resultsql.add((Integer)data[0]);
 		}
 		
 		while(loop1)
 		{
 			Integer id = (Integer) oluc.next();
 
 			if(id instanceof Integer)
 			{
 				for(int i = 0; i < resultsql.size() ; i++)
 				{
 					if(id.equals((Integer) resultsql.get(i)))
 						result.add(resultsql.get(i));
 				}
 			}
 			else
 				loop1 = false;
 		}
 		
 		index = 0;
 	}
 
 	public Object next() {
 		index++;
 		if(index > result.size())
 			return null;
 		System.out.println("ici result : "+result.get(index-1));
 		return result.get(index-1);
 		
 	}
 
 }