package operator;

import java.util.ArrayList;
import java.util.List;

public class OperatorLuceneSQL  implements OperatorInterface
{
	OperatorSQL osql;
	OperatorLucene oluc;
	String querySQL;
	String queryLucene;
	List<Object> result;
	int index;
	
	public void init(String query) 
	{
		boolean loop1 = true;
		boolean loop2 = true;
		String list[] = query.split("with");
		
		querySQL = list[0].trim();
		queryLucene = list[1].trim();

		long startTime = System.currentTimeMillis();

		oluc = new OperatorLucene();
		oluc.init(queryLucene);
		
		result = new ArrayList<Object>();
		// join


		while(loop2)
		{
			Integer id = (Integer) oluc.next();
			loop1=true;
			
			if(id==null)
				loop2 = false;
			else
			{
				osql = new OperatorSQL();
				osql.init(querySQL);

				while(loop1)
				{
					Object obj = osql.next();
					Object[] data = (Object[]) obj;

					if(obj == null)
						loop1 = false;
					else if(id.equals((Integer)data[0]))
						result.add(obj);
				}
			}
		}
		

		long endTime = System.currentTimeMillis();
		System.out.println("Requête mixte - plan d'execution 2, time taken: "
				+(endTime-startTime)+" ms");

		index = 0;
	}

	public Object next() {
		index++;
		if(index > result.size())
			return null;
		System.out.println("ici result : "+((Integer) ((Object[])result.get(index-1))[0]));
		return result.get(index-1);
		
	}

}
