package operator;

import java.io.IOException;

import org.apache.lucene.queryParser.ParseException;

import lucene.LuceneDocument;
import lucene.data.TouristAttractionLucene;

public class OperatorLucene implements OperatorInterface 
{
	private boolean initok;
	private LuceneDocument luceneDoc;
	
	public void init(String query) 
	{
		luceneDoc = new TouristAttractionLucene();

		try 
		{
			luceneDoc.init(query);
			initok = true;
		} 
		catch (IOException e) 
		{
			initok = false;
			e.printStackTrace();
		} 
		catch (ParseException e) 
		{
			initok = false;
			e.printStackTrace();
		}
	}

	
	public Object next() 
	{
		if(initok)
			initok = luceneDoc.hasNext();
		
		if(initok)
			return luceneDoc.next();
		else
			return null;
	}


	public OperatorLucene() 
	{
		super();
		initok = false;
	}
	
}
