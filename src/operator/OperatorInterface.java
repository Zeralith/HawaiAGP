package operator;

public interface OperatorInterface {
	public void init(String query);
	public Object next();

}
